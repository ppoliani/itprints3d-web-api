// region Import

var fs          = require('fs'),
    Promise     = require('bluebird'),
    lwip        = require('lwip'),
    uof         = require('../../data/unitOfWork'),
    utils       = require('../utils.js');


// endregion

// Promisisfication
Promise.promisifyAll(lwip);

// region Inner Fields

var imageRepository = uof.repository('Printer_Image');

// endregion

// region Inner Methods

/**
 * Finds the image with the given id resizes it and streams it into
 * the response stream
 * @param id
 * @param width
 * @param height
 * @param res
 * @private
 */
function _findResizeImg(id, width, height, res){
    imageRepository.findById(id)
        .then(function(img){
            return lwip.openAsync(img.data, 'jpg');
        })
        .then(function(image){
            Promise.promisifyAll(image);

            return image.resizeAsync(width, height);
        })
        .then(function(rszImg){
            return rszImg.toBufferAsync('jpg');
        })
        .then(function(image){
            res.send(image);
        })
        .catch(function(err){
            res.status(404).send('Image not found: ' + err);
        });
}

/**
 * Reads the image dimension form the request object
 * @param req
 * @private
 */
function _getImageDims(req){
    var width = Number(req.query.width) || 100,
        height = Number(req.query.height) || 100;

    return {
        width: width,
        height: height
    };
}

/**
 * Returns the repository for the given type of product
 * @param productType
 * @private
 */
function _getProductsRepository(productType){
    return  uof.repository(productType);
}

// endregion

// region Endpoints

/**
 * GET /images
 */
function getImages(req, res){
    var
        query = imageRepository.query()
            .where('userID').equals(req.user.id)
            .skip(req.query.$skip)
            .limit(req.query.$top)
            .select('id name'),

        countQuery =  imageRepository.query()
            .where('userID').equals(req.user.id);

    utils.makePaginationQuery(query, countQuery, res);
}

/**
 * GET /images/:id
 */
function getImageById(req, res){
    var dims = _getImageDims(req);

    _findResizeImg(req.params.id, dims.width, dims.height, res);
}

/**
 * POST /images
 */
function postImage(req, res){
    fs.readFile(req.files.file.path, function(err, data) {
        if (err) {
            throw err;
        }

        var image = {
            name: req.files.file.originalname,
            data: data,
            contentType: 'image/png',
            userID: req.user.id
        };

        imageRepository.save(image)
            .then(function(){
                res.send(200, 'Image Added');
            })
            .catch(function(err){
                throw err;
            });
    });
}

/**
 * GET /printers/:id/image
 */
function getProductImage(req, res){
    var dims = _getImageDims(req),
        productRepository = _getProductsRepository(req.query.product);

    productRepository.findById(req.params.id)
        .then(function(printer){
            _findResizeImg(printer.printer_ImageID[0], dims.width, dims.height, res);
        })
        .catch(function(err){
            res.status(404).send('Printer not found: ' + err);
        });
}

// endregion

// region Export

module.exports = {
    getImages: getImages,
    getImageById: getImageById,
    postImage: postImage,
    getProductImage: getProductImage
};

// endregion