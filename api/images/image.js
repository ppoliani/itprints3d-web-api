// region Import

var mongoose = require('mongoose');

// endregion

var
    Schema = mongoose.Schema,

    Image =  new Schema({
        name: String,
        data: Buffer,
        contentType: String,
        userID: Schema.Types.ObjectId
    }),

// split images into different collections
PrinterImageModel = mongoose.model('Printer_Image', Image),
ScannerImageModel = mongoose.model('Scanner_Image', Image),
ArticleImageModel = mongoose.model('Article_Image', Image);

Image.method('serialize', function serialize(ret){
    ret.id = ret._id.toString();
    ret.name = ret.name;
    ret.contentType = ret.contentType
    ret.userID = ret.userID;

    delete ret._id;

    return ret;
});

Image.set('toJSON', {
    transform: function(doc, ret){
        ret.id = ret._id.toString();
        delete ret._id;

        return ret;
    }
});

// region Export

exports.PrinterImage = PrinterImageModel;
exports.ScannerImage= ScannerImageModel;
exports.ArticleImage = ArticleImageModel;

// endregion