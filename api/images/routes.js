var imagesAPI = require('./imagesAPI');

module.exports = {
    'images': {
        method: 'get',
        fn: imagesAPI.getImages,
        auth: { scope: '*', claims: { role: ['admin'] } }
    },

    'images/:id': {
        method: 'get',
        fn: imagesAPI.getImageById
    },

    '$images': {
        method: 'post',
        fn: imagesAPI.postImage,
        auth: { scope: '*', claims: { role: ['admin'] } }
    },

    'products/:id/image': {
        method: 'get',
        fn: imagesAPI.getProductImage
    }
};