// region Import

var passport            = require('passport'),
    apiEndpoint         = require('oauth-api-endpoint'),
    security            = require('../security'),
    _                   = require('lodash');


// endregion

// region Consts

var BASE_URL = '/api/v1/';

// endregion

// region Inner Fields

var routes = [
    require('./images').routes,
    require('./printers').routes,
    require('./subscription').routes,
    require('../e-commerce/routes'),
    require('../breeze-mongoose/routes')
];

// endregion

// region Inner Methods

/**
 * Sets up all routes
 * @param app
 */
function setupRoutes(app){
    var api = apiEndpoint(app, passport);

    routes.forEach(function(route){
        _.forOwn(route, function(endpoint, path){
            if(endpoint.auth){ // bearer middleware
                api[endpoint.method](_pathFormatter(path), endpoint.auth, endpoint.fn);
            }
            else {
                app[endpoint.method](_pathFormatter(path), endpoint.fn);
            }
        });
    });
}

/**
 * Gets the path for the given resource
 * @param resouce
 * @returns {string}
 * @private
 */
function _pathFormatter(resource){
    resource = resource[0] === '$' ? resource.split("$").join("") : resource;

    return BASE_URL + resource;
}

// endregion

// region Export

module.exports = setupRoutes;

// endregion