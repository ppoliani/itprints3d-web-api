// region Import

var mongoose      = require('mongoose'),
    extend        = require('mongoose-schema-extend'),
    ProductSchema = require('../../e-commerce/models/product');

// endregion

var
    Schema = mongoose.Schema,

    PrinterSchema = ProductSchema.extend({
        weight: { // metric unit
            type: Number
        },

        dimension: {
            width: { type: Number },
            height: { type: Number },
            depth: { type: Number }
        },

        layerThickness: {
            type: Number
        },

        powerRequirements: {
            type: String
        },

        warranty: {
            type: Number // Months
        },

        connectivity: {
            wireless: String,
            wired: String,
            mobileDevices: String
        },

        buildVolume: {
            width: Number,
            height: Number,
            depth: Number
        },

        printingTechnology: {
            type: String
        },

        supportMaterial: {
            type: String
        },

        extraFeatures: {
            type: String
        },

        userID: Schema.Types.ObjectId,

        printer_ImageID: [String]
    }),

    PrinterModel = mongoose.model('Printer', PrinterSchema);

PrinterSchema.method('serialize', function serialize(ret){
    ret.id = ret._id.toString();
    ret.dimension = ret.dimension && ret.dimension[0];
    ret.connectivity = ret.connectivity && ret.connectivity[0];
    ret.buildVolume = ret.buildVolume && ret.buildVolume[0];

    delete ret._id;

    return ret;
});

PrinterSchema.set('toJSON', {
    transform: function(doc, ret, options){
        return PrinterSchema.methods.serialize(ret);
    }
});

// region Export

module.exports = PrinterModel;

// endregion