// region Import

var utils       = require('../utils.js'),
    uof         = require('../../data/unitOfWork');

// endregion

// region Inner Fields

var printerRepository = uof.repository('Printer');

// endregion

// region Inner Methods

/**
 * GET /printers
 */
function getPrinters(req, res){
    printerRepository.query()
        .exec()
        .then(function(printers){
            res.json(utils.createResultObject(printer.length, printers));
        })
        .catch(function(err){
            res.json(utils.createErrorResult('Could not fetch the printers', err.message));
        });
}

/**
 * GET /printers/lookups
 */
function lookups(req, res){
    var orderBy = req.query.$orderby,
        query = printerRepository.query()
            .skip(req.query.$skip)
            .limit(req.query.$top)
            .select('brandName productName description price printer_ImageID'),

        countQuery =  printerRepository.query();

    if(orderBy){
        query = query.sort(orderBy);
    }

    utils.makePaginationQuery(query, countQuery, res);
}

/**
 * GET /printers/suggested/lookups
 */
function suggestedlookups(req, res){
    // ToDo: add logic to get the suggested printers and not any random
    var query = printerRepository.query()
            .skip(req.query.$skip)
            .limit(req.query.$top)
            .select('brandName productName description price printer_ImageID'),

        countQuery =  printerRepository.query();

    utils.makePaginationQuery(query, countQuery, res);
}

/**
 * GET /printers/:id
 */
function getPrinterById(req, res){
    printerRepository.findById(req.params.id)
        .then(function(printer){
            if(!printer){
                res.send(404);
            }
            else {
                res.json(utils.createResultObject(undefined, printer));
            }
        })
        .catch(function(err){
            res.json(utils.createErrorResult('An error occured while fetching the printer: ', err.message));
        });
}

/**
 * GET /admin/printers/lookups
 */
function adminLookups(req, res){
    var query = printerRepository.query()
            .where('userID').equals(req.user.id)
            .skip(req.query.$skip)
            .limit(req.query.$top)
            .select('brandName productName printer_ImageID price stockAvailability'),

        countQuery =  printerRepository.query()
            .where('userID').equals(req.user.id);

    utils.makePaginationQuery(query, countQuery, res);
}

// endregion

// region Export

module.exports = {
    getPrinters: getPrinters,
    lookups: lookups,
    adminLookups: adminLookups,
    suggestedlookups: suggestedlookups,
    getPrinterById: getPrinterById
};

// endregion