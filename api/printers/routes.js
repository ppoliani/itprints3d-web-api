var printerAPI = require('./printerAPI');

module.exports = {
    'printers': {
        method: 'get',
        fn: printerAPI.getPrinters,
        auth: { scope: '*', claims: { role: ['admin'] } }
    },

    'printers/suggested/lookups': {
        method: 'get',
        fn: printerAPI.suggestedlookups
    },

    'admin/printers/lookups': {
        method: 'get',
        fn: printerAPI.adminLookups,
        auth: { scope: '*', claims: { role: ['admin'] } }
    },

    'printers/lookups': {
        method: 'get',
        fn: printerAPI.lookups
    },

    'printers/:id': {
        method: 'get',
        fn: printerAPI.getPrinterById
    }
};