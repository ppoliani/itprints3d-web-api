module.exports = {
    routes: require('./routes'),
    api: require('./subscriptionAPI')
};