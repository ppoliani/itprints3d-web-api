var subscriptionAPI = require('./subscriptionAPI');

module.exports = {
    'subscribe': {
        method: 'post',
        fn: subscriptionAPI.subscribe,
        auth: { scope: '*' }
    }
};