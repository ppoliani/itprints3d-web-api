// region Import

var mongoose = require('mongoose');

// endregion

// region Inner Methods

var Subscription = new mongoose.Schema({
    email: {
        type: String,
        value: String
    }
});

var Model = mongoose.model('Subscription', Subscription);

// endregion

// region Export

exports.schema = Subscription;
exports.model = Model;

// endregion