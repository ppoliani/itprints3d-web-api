
// region Import

var Subscription = require('../../data/index').getModel('Subscription');
//    SubscriptionShema = require('../../data/subscription').schema;

// endregion

// region Inner Methods

/**
 * POST /subscribe
 */
function subscribe(req, res){
    var subscription = new Subscription({ email: req.body.email });

    subscription.save(function(err, docs){
        if(err){
            res.send(404, err);
        }
        else{
            res.send(200);
        }
    });
}

//SubscriptionShema.pre("save", function(next) {
//    var self = this;
//
//    Subscription.findOne({email : this.email}, 'email', function(err, results) {
//        if(err) {
//            next(err);
//        } else if(results) {
//            console.warn('results', results);
//            self.invalidate("email", "email must be unique");
//            next(new Error("email must be unique"));
//        } else {
//            next();
//        }
//    });
//});


// endregion

// region Export

module.exports = {
  subscribe: subscribe
};

// endregion