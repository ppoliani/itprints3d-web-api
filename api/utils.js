// region Import

var async = require('async');

// endregion

// region Inner Methods

/**
 * Creates an API result object that will contain the
 * given list of items
 * @param items
 * @param page
 */
function createResultObject(totalItems, items, page){
    return {
        total: totalItems,
        page: page,
        items: items
    };
}

/**
 * Creates an error result object
 * @param error
 * @returns {{error: *}}
 */
function createErrorResult(message, error){
    return {
        message: message,
        error: error ? error : message
    };
}

/**
 * Executes an additional query to get the total items that is
 * needed for pagination
 * @param query
 * @param countQuery
 * @param res
 */
function makePaginationQuery(query, countQuery, res){
    async.parallel({
        entities: function(clb){
            query.exec()
                .then(function(_entities){
                    clb(null, _entities);
                })
                .catch(function(err){
                    throw err;
                });
        },

        count: function(clb){
            countQuery.exec()
                .then(function(_entities){
                    clb(null, _entities.length);
                })
                .catch(function(err){
                    throw err;
                });
        }
    }, function(err, results){
        if(err) throw err;

        res.json(createResultObject(results.count, results.entities));
    });
}

/**
 *
 * @param clb
 */
function asyncTask(clb){
    clb = _.isFunction(clb) ? clb : function() {};
}

// endregion

// region Export

module.exports = {
    createResultObject: createResultObject,
    makePaginationQuery: makePaginationQuery,
    createErrorResult: createErrorResult,
    asyncTask: asyncTask
};

// endregion