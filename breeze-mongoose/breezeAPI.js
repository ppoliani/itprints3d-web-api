// region Import

var breezeMongoose = require('breeze-mongoose')(require('../data').getModel),
    db             = require('../data/index');

// endregion

// region Inner Methods

/**
 * GET /breeze/metadata
 */
function getMetadata(req, res){
    res.json(breezeMongoose.getMetadata(db.getAllSchemas()));
}

/**
 * POST /savechanges
 */
function saveChanges(req, res){
    breezeMongoose.saveChanges(req.body)
        .then(function(saveResults){
            res.json(saveResults);
        })
        .catch(function(message){
            res.send(500, message);
        });
}

// endregion

// region Export

module.exports = {
    getMetadata: getMetadata,
    saveChanges: saveChanges
};

// endregion