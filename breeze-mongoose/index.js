
// region Import

var metadataGenerator = require('./metadataGenerator.js'),
    dbManager         = require('./dbManager.js');

// endregion



// region Export

module.exports = {
    getMetadata: metadataGenerator.getMetadata,
    saveChanges: dbManager.saveChanges
};

// endregion