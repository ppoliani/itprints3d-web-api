var breezeAPI = require('./breezeAPI');

module.exports = {
    'breeze/metadata': {
        method: 'get',
        fn: breezeAPI.getMetadata
    },

    'savechanges': {
        method: 'post',
        fn: breezeAPI.saveChanges,
        auth: { scope: '*' }
    }
};