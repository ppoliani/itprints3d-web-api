
//region Import

var nconf = require('nconf');

//endregion

//region Module

nconf.argv()
    .env()
    .file({ file: './config.json' });

//endregion

//region Export

module.exports = nconf;

//endregion