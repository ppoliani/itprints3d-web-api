module.exports = {
    User: require('./../security/user'),
    AccessToken: require('./../security/accessToken'),
    Claim: require('./../security/claim').ClaimModel
};