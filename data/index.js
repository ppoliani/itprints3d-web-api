//region Import

var mongoose            = require('mongoose'),
    config              = require('../config/index'),
    log                 = require('../config/logger')(module),
    models              = require('./models'),
    authModels          = require('./authModels');

//endregion

// region Private Fields

var
    mongodb_main_connection_string = process.env.NODE_ENV === 'test'
        ? config.get('mongoose:testURI')
        : config.get('mongoose:mainURI'),

    mongodb_auth_connection_string = config.get('mongoose:authURI');

if(process.env.OPENSHIFT_MONGODB_DB_URL){
    // ToDo: use this when mongodb used on openshift
    // mongodb_main_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + 'api';
    // mongodb_auth_connection_string = config.get('mongoose:openshift');
}

if(process.env.OPENSHIFT_NODEJS_PORT){
    mongodb_main_connection_string = 'mongodb://admin:MYFrEimLrTQ1@kahana.mongohq.com:10062/api';
    mongodb_auth_connection_string = 'mongodb://admin:MYFrEimLrTQ1@kahana.mongohq.com:10061/oauth';
}

var mainConnection = mongoose.createConnection(mongodb_main_connection_string),
    authConnection = mongoose.createConnection(mongodb_auth_connection_string);


// endregion

// region Inner Methods

/**
 * Initializes db
 */
function initDb(){
    _connectToAuthDB();
    _connectToMainDB();
    _registerModels();
}

/**
 * Returns the given mongoose model
 * @param model
 */
function getModel(model){
    return mainConnection.model(model);
}

/**
 * Returns the given mongoose auth-related model
 * @param model
 */
function getAuthModel(model){
    return authConnection.model(model);
}

/**
 * Returns all the model definitions
 */
function getAllSchemas(){
    var models = mainConnection.models;

    models.User = getAuthModel('User');
    return models;
}

/**
 * Clears all the collections
 */
function clearCollections(){
    var mainConnectionCollections = mainConnection.collections;

    for (var collection in mainConnectionCollections) {
        mainConnectionCollections[collection].remove(function(err){
            if(err) throw err;
        });
    }
}

/**
 * Returns the number of active connections
 * @returns {*}
 */
function getNumOfActiveConnections(){
    return mongoose.connections.length;
}

/**
 * Connects to the auth db
 */
function _connectToAuthDB(){
    authConnection.on('error', function (err) {
        log.error('connection error:', err.message);
    });
    authConnection.once('open', function callback () {
        log.info("Connected to auth DB!");
    });
}

/**
 * Connects to the main db
 */
function _connectToMainDB(){
    mainConnection.on('error', function (err) {
        log.error('connection error:', err.message);
    });
    mainConnection.once('open', function callback () {
        log.info("Connected to main DB!");
    });
}

/**
 * Registers mongoose models
 * @private
 */
function _registerModels(){
    authConnection.model('User', authModels.User);
    authConnection.model('AccessToken', authModels.AccessToken);
    authConnection.model('Claim', authModels.Claim);

    mainConnection.model('Printer', models.Printer);
    mainConnection.model('Printer_Image', models.ImageModels.PrinterImage);
    mainConnection.model('Scanner_Image', models.ImageModels.PrinterImage);
    mainConnection.model('Article_Image', models.ImageModels.PrinterImage);
    mainConnection.model('Subscription', models.Subscription);

    // e-commerce
    mainConnection.model('Product', models.Product);
    mainConnection.model('ShoppingCart', models.ShoppingCart);
    mainConnection.model('CheckoutProcess', models.CheckoutProcess);
    mainConnection.model('ReservedProduct', models.ReservedProduct);
    mainConnection.model('Order', models.Order);
}

// endregion

// region Export

module.exports = {
    initDb: initDb,
    getModel: getModel,
    getAuthModel: getAuthModel,
    getAllSchemas: getAllSchemas,
    clearCollections: clearCollections,
    getNumOfActiveConnections: getNumOfActiveConnections
};

// endregion
