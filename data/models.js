
module.exports = {
    Printer: require('./../api/printers/printer'),
    ImageModels: require('./../api/images/image'),
    Subscription: require('./../api/subscription/subscription').model,

    // e-commerce
    Product: require('../e-commerce/models/product'),
    ShoppingCart: require('../e-commerce/models/shoppingCart'),
    CheckoutProcess: require('../e-commerce/models/checkoutProcess'),
    ReservedProduct: require('../e-commerce/models/reservedProduct'),
    Order: require('../e-commerce/models/order')
};