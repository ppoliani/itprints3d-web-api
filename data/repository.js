// region Import

var Promise         = require('bluebird'),
    _               = require('lodash'),
    db              = require('./index'),
    RepositoryQuery = require('./repositoryQuery'),
    TTL             = require('./ttl');

// endregion

// region Inner Methods

/**
 * Adds ttl functionality to the collection
 * @param ttlOpts
 */
function setTTL(ttlOpts){
    this._ttl = new TTL(this, ttlOpts);
}

/**
 * Returns the entity with the given id
 * @param id
 */
function findById(id){
    return new Promise(function(resolve, reject){
        this.Model.findById(id, function(err, entity){
            if(err){ reject(err); }
            else { resolve(entity); }
        });
    }.bind(this));
}

/**
 * Creates a new entity with the given values and saves it to the db
 * @param rawObject
 */
function save(rawObject){
    var entity = new this.Model();
    entity = _copy(rawObject, entity);

    function _copy(rawObject, copyTo) {
        _.forOwn(rawObject, function(value, key){
            if(_.isPlainObject(value)) {
                copyTo[key] = _copy(value, copyTo[key]);
            }
            else if(_.isArray(value)){
                value.forEach(function(item){
                    copyTo[key].push(item);
                });
            }
            else {
                copyTo[key] = value;
            }
        });

        return copyTo;
    }

    return new Promise(function(resolve, reject){
        entity.save(function(err, entity){
            if(err){ reject(err); }
            else { resolve(entity); }
        });
    });
}

/**
 * Finds and updates the doc with the given id
 * @param id
 */
function findByIdAndUpdate(id){
    return new Promise(function(resolve, reject){
        this.Model.findByIdAndUpdate(id, function(err){
            if(err){ reject(err); }
            else{ resolve(); }
        });
    }.bind(this));
}

/**
 * Finds and removes the document with the given id
 * @param id
 */
function findByIdAndRemove(id){
    return new Promise(function(resolve, reject){
        this.Model.findByIdAndRemove(id, function(err){
            if(err){ reject(err); }
            else{ resolve(); }
        });
    }.bind(this));
}

/**
 * Return a new instance of the repository query class
 */
function query(){
    return new RepositoryQuery(this);
}

/**
 * Will execute and return the query with the given options
 * @param opts
 */
function get(opts){
    return new Promise(function(resolve, reject){
        var query = this.Model.find();

        if(opts.options){
            query = query.setOptions(opts.options);
        }

        if(opts.where){
            query = query.where(opts.where);
        }

        if(opts.equals){
            query = query.equals(opts.equals);
        }

        if(opts.skip){
            query = query.skip(opts.skip);
        }

        if(opts.limit){
            query = query.limit(opts.limit);
        }

        if(opts.select){
            query = query.select(opts.select);
        }

        if(opts.sort){
            query = query.sort(opts.sort);
        }

        if(opts.$or){
            query = query.or(opts.$or)
        }

        if(opts.$and){
            query = query.and(opts.$and)
        }

        if(opts.$in){
            query = query.in(opts.$in)
        }

        if(opts.gt){
            query = query.gt(opts.gt)
        }

        if(opts.gte){
            query = query.gte(opts.gte)
        }

        if(opts.lt){
            query = query.lt(opts.lt)
        }

        if(opts.lte){
            query = query.lte(opts.lte)
        }

        if(opts.update){
            // add a callback to ensure safe updates; exec() will result in
            // and unsafe write. consult the mongoose docs
            query.update(opts.update, function(err, numOfAffectedDocs){
                if(err){
                    reject(err);
                }
                else {
                    resolve(numOfAffectedDocs);
                }
            });

            return;
        }

        if(opts.remove){
            // ensure save operation by providing a callback
            query.remove(null, function(err, numOfAffectedDocs){
                if(err){
                    reject(err);
                }
                else {
                    resolve(numOfAffectedDocs);
                }
            });

            return;
        }

        query.exec(function(err, entities){
            if(err){ reject(err); }
            else { resolve(entities); }
        });
    }.bind(this));
}

// endregion

// region Ctor

var Repository = function Repository(name){
    this.name = name;
    this.Model = db.getModel(name);
    this._ttl = null;
};

Repository.prototype = {
    constructor: Repository,
    setTTL: setTTL,
    findById: findById,
    save: save,
    findByIdAndUpdate: findByIdAndUpdate,
    findByIdAndRemove: findByIdAndRemove,
    query: query,
    get: get
};

// endregion

// region Export

module.exports = Repository;

// endregion