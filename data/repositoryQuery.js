// region Import

// endregion

// region Inner Methods

function setOptions(options){
    this._options = options;

    return this;
}

function where(expression){
    this._where = expression;

    return this;
}

function equals(expression){
    this._equals = expression;

    return this;
}

function skip(_skip){
    this._skip = _skip;

    return this;
}

function limit(_limit){
    this._limit = _limit;

    return this;
}

function select(expression){
    this._select = expression;

    return this;
}

function update(expression){
    this._update = expression;

    return this;
}

function remove(){
    this._remove = true;

    return this;
}

function sort(expression){
    this._sort = expression;

    return this;
}

function $or(expression){
    this._or = expression;

    return this;
}

function $and(expression){
    this._and = expression;

    return this;
}

function $in(expression){
    this._in = expression;

    return this;
}

function gt(condition){
    this._gt = condition;

    return this;
}

function gte(condition){
    this._gte = condition;

    return this;
}

function lt(condition){
    this._lt = condition;

    return this;
}

function lte(condition){
    this._lte = condition;

    return this;
}

function exec(){
    return this._repository.get({
        options: this._options,
        where: this._where,
        equals: this._equals,
        skip: this._skip,
        limit: this._limit,
        select: this._select,
        update: this._update,
        remove: this._remove,
        sort: this._sort,
        $or: this._or,
        $and: this._and,
        $in: this._in,
        gt: this._gt,
        gte: this._gte,
        lt: this._lt,
        lte: this._lte
    });
}

// endregion

// region Ctor

var RepositoryQuery = function RepositoryQuery(repository){
    this._repository = repository;
    this._options = null;
    this._where = null;
    this._equals = null;
    this._skip = null;
    this._limit = null;
    this._select = null;
    this._update = null;
    this._remove = false;
    this._sort = null;
    this._gt = undefined;
    this._gte = undefined;
    this._lt = undefined;
    this._lte = undefined;
    this._or = null;
    this._and = null;
};


RepositoryQuery.prototype = {
    constructor: RepositoryQuery,
    setOptions: setOptions,
    where: where,
    equals: equals,
    skip: skip,
    limit: limit,
    select: select,
    update: update,
    remove: remove,
    sort: sort,
    $or: $or,
    $and: $and,
    $in: $in,
    gt: gt,
    gte: gte,
    lt: lt,
    lte: lte,
    exec: exec
};

// endregion

// region Export

module.exports = RepositoryQuery;

// endregion