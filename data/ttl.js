// region Import

// endregion

// region Consts

var DEFAULT_TIME_INTERVAL = 300; // 5 mins

// endregion

// region Inner fields


// endregion

// region Inner Methods

/**
 * Tick function; triggeres every X mins
 * @private
 */
function _tick(){
    this._repository.query()
        .exec()
        .then(_removeExpiredDocs.bind(this))
        .catch(function(err){
            throw err;
        });
}

/**
 * Removes all the expired documents
 * @param docs
 * @private
 */
 function _removeExpiredDocs(docs){
    var expiresAfter = this._options.expiresAfter,
        property = this._options.property,
        onRemove = this._options.onRemove,
        canRemove = this._options.canRemove;

    docs.forEach(function(doc){
        if(_docExpired(doc[property], expiresAfter)){
            canRemove(doc)
                .then(function(result){
                    if(result){
                        doc.remove(function(err, _doc_){
                            if(err) throw err;

                            // notification
                            onRemove(_doc_);
                        });
                    }
                });
        }
    }.bind(this));
}

/**
 * Returns true if the given document has expires
 * @param createdAt
 * @param expiresAfter
 * @private
 */
 function _docExpired(createdAt, expiresAfter){
    if(createdAt instanceof Date){
        return Math.round((Date.now() - createdAt) / 1000) > expiresAfter;
    }
    else {
        throw new Error('Property is not of Date type');
    }
}

// endregion

// region Ctor

var TTL = function TTL(repository, options){
    this._repository = repository;
    this._options = options;

    setInterval(_tick.bind(this), (options.tickInterval || DEFAULT_TIME_INTERVAL) * 1000);
};

// endregion

// region Export

module.exports = TTL;

// endregion