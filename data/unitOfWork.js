// region Import

var Promise    = require('bluebird'),
    Repository = require('./repository');

// endregion

// region Inner Methods

var _repositories = {
    Printer: new Repository('Printer'),
    Printer_Image: new Repository('Printer_Image'),
    ShoppingCart: new Repository('ShoppingCart'),
    CheckoutProcess: new Repository('CheckoutProcess'),
    ReservedProduct: new Repository('ReservedProduct'),
    Order: new Repository('Order')
};

// endregion

// region Inner Methods

/**
 * Returns the repository with the given name
 * @param type
 */
function repository(type){
    return _repositories[type];
}

/**
 * Returns a a function that executes the given action on the given entity
 * @param action
 * @private
 */
function _getEntityAction(action){
    return function(entity) {
        return new Promise(function(resolve, reject){
            entity[action](function (err, _entity_) {
                if (err) { reject(err) }
                else { resolve(_entity_) }
            });
        });
    }
}

// endregion

// region Export

module.exports = {
    repository: repository,
    save: _getEntityAction('save'),
    remove: _getEntityAction('remove')
};

// endregion