var Enum = require('../utils/enum');

module.exports = new Enum({
    AMERICAN_EXPRESS: 0,
    DELTA: 10,
    MASTERCARD: 20,
    SOLO: 30,
    MAESTRO: 40,
    VISA: 50,
    VISA_ELECTRON: 60
});
