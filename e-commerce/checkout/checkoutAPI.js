// region Import

var async                   = require('async'),
    utils                   = require('../../api/utils.js'),
    checkoutManager         = require('./checkoutManager');

// endregion

// region API Endpoints

/**
 * POST /checkout
 */
function checkout(req, res){
    var execSeq = async.compose(
            checkoutManager.reserveProducts,
            checkoutManager.checkProductAvailability,
            checkoutManager.getShoppingCart,
            checkoutManager.checkIfCheckoutExist
    );

    execSeq(req.body.cartID, function(err, cart){
        if(err){
            res.json(utils.createErrorResult(err._message_, err.content));
        }
        else {
            checkoutManager.saveCheckoutProcess(cart.id)
                .then(function(checkoutProcess){
                    res.send(200, checkoutProcess);
                })
                .catch(function(err){
                    res.json(utils.createErrorResult('Could not save the current checkout process', err));
                });
        }
    });
}

// endregion

// region Export

module.exports = {
    checkout: checkout
};

// endregion