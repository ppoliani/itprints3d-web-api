// region Import

var Promise             = require('bluebird'),
    uof                 = require('../../data/unitOfWork'),
    stockManager        = require('../stock/stockManager'),
    logger              = require('../../logging/logger'),
    checkoutPhaseEnum   = require('../checkoutPhaseEnum');

// endregion

// region Consts

var TTL_EXPIRATION = 3600; // one hour

// endregion

// region Inner Fields

var shoppingCartRepository = uof.repository('ShoppingCart'),
    checkoutRepository = uof.repository('CheckoutProcess');

// endregion

// region Inner Methods

/**
 * Checks if the checkout for the given cart has already been started
 * @param cartID
 * @param clb
 * @private
 */
function checkIfCheckoutExist(cartID, clb){
    checkoutRepository.query()
        .where('cartID').equals(cartID)
        .exec()
        .then(function(checkouProcess){
            if(checkouProcess[0]){
                clb({
                    _message_: 'Checkout process has already been started',
                    content: {
                        createdAt: checkouProcess.createdAt
                    }
                });
            }
            else{
                clb(null, cartID);
            }
        })
        .catch(function(err){
            err._message_ = 'Error while reading the checkout process for the given cartID: ' + cartID;
            clb(err);
        });
}

/**
 * returns the shopping cart with the given user id exist
 * @param cartID
 * @private
 */
function getShoppingCart(cartID, clb){
    shoppingCartRepository.findById(cartID)
        .then(function(cart){
            clb(null, cart);
        })
        .catch(function(err){
            err._message_ = 'Error while getting an existing shopping cart';
            clb(err);
        });
}

/**
 * Checks the stock for each product in the cart
 * @param cart
 * @private
 */
function checkProductAvailability(cart, clb){
    stockManager.checkIfProductsInStock(cart.cartProducts)
        .then(function(errors){
            if(!errors.length){
                clb(null, cart);
            }
            else {
                clb({
                    _message_: 'Stock Availability errors',
                    content: errors
                });
            }
        })
        .catch(function(err){
            err._message_ = 'Error while checking product availability';
            clb(err);
        });
}

/**
 * Will mark the products of the cart as reserved
 * @param cart
 * @param clb
 * @private
 */
function reserveProducts(cart, clb){
    stockManager.reserveProducts(cart.cartProducts)
        .then(function(){
            clb(null, cart);
        })
        .catch(function(err){
            err._message_ = 'Could not reserve products from the cart';
            clb(err);
        });
}

/**
 * Saves the given checkout process to the db
 * @param cartID
 * @private
 */
function saveCheckoutProcess(cartID){
    var checkoutProcess = {
        cartID: cartID
    };

    return checkoutRepository.save(checkoutProcess);
}

// endregion

// region Events

/**
 * Triggered when a checkout doc has expired
 * @param doc
 */
function on_docRemoved(doc){
    logger.info('Removing expired checkout processes');

    getShoppingCart(doc.cartID, function(err, shoppingCart){
        stockManager.removeReservedProducts(shoppingCart.cartProducts)
            .catch(function(err){
                err._message_ = "Error while removing the reserved products associated with the cart: " + shoppingCart.id;
                throw err;
            });
    });
}

// endregion

// region TTL

/**
 * Checks if the given doc can be removed;
 * It cannot be removed if the checkout process
 * is in the payment phase
 * @param doc
 */
function _canRemove(doc){
    return new Promise(function(resolve){
        resolve(doc.phase !== checkoutPhaseEnum.PAYMENT);
    });
}

checkoutRepository.setTTL({
    property: 'createdAt',
    expiresAfter: TTL_EXPIRATION,
    onRemove: on_docRemoved,
    canRemove: _canRemove
});

// endregion

// region Export

module.exports = {
    checkIfCheckoutExist: checkIfCheckoutExist,
    getShoppingCart: getShoppingCart,
    checkProductAvailability: checkProductAvailability,
    reserveProducts: reserveProducts,
    saveCheckoutProcess: saveCheckoutProcess
};

// endregion