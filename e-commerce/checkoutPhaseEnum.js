var Enum = require('../utils/enum');

module.exports = new Enum({
    START: 0,
    PAYMENT: 10,
    SUCCESS: 20,
    FAIL: 30
});