// region Import

var async        = require('async'),
    Promise      = require('bluebird'),
    uof          = require('../data/unitOfWork'),
    logger       = require('../logging/logger'),
    stockManager = require('./stock/stockManager');

// endregion

// region Inner Fields

var shoppingCartRepository = uof.repository('ShoppingCart'),
    checkoutRepository = uof.repository('CheckoutProcess');


// endregion

// region Inner Methods

/**
 * Removes the shopping cart with the given id, and
 * the both the reserved products and checkout process
 * associated with it
 * @param cartID
 */
function cleanUp(cartID){
    return new Promise(function(resolve, reject){
        shoppingCartRepository.findById(cartID)
            .then(function(shoppingCart){
                if(!shoppingCart){
                    reject(new Error('No Shopping cart with the given cart id was found: ' + cartID));
                }
                else{
                    return shoppingCart;
                }
            })
            .then(function(shoppingCart){
                return removeCheckoutProcesses(shoppingCart.id, shoppingCart.cartProducts);
            })
            .then(function(){
                shoppingCartRepository.findByIdAndRemove(cartID)
                    .then(function(){
                        logger.log('The shopping cart [' + cartID + '] was removed along with the related data i.e. reserved products and checkout process ');

                        resolve();
                    })
                    .catch(function(err){
                        reject(err);
                        logger.error('Error while removing the shopping cart {' + cartID + ']');
                    });
            })
            .catch(function(err){
                reject(err);
            });
    });
}

/**
 * Removes a checkout process when the corresponding shopping cart is removed
 * @param doc
 */
function removeCheckoutProcesses(shoppingCartID, cartProducts){
    return new Promise(function(resolve, reject){
        var execAsync = async.compose(
            _removeCheckoutProcess.bind(null, shoppingCartID),
            _removeReservedProducts
        );

        execAsync(cartProducts, function(err){
            if(err){ reject(err); }
            else { resolve(); }
        });
    });
}

/**
 * Removes the checkout process document associated with the given shopping cart id
 * @param shoppingCartID
 * @param clb
 * @private
 */
function _removeCheckoutProcess(shoppingCartID, clb){
    checkoutRepository.query()
        .where('cartID').equals(shoppingCartID)
        .remove()
        .exec()
        .then(function(){
            clb(null);
        })
        .catch(function(err){
            err._message_ = "Error while removing checkout process associated with the cart: " + shoppingCartID;
            throw err;
        });
}

/**
 * Removes the reserved products associated with a shopping cart
 * @param shoppingCartProducts
 * @param clb
 * @private
 */
function _removeReservedProducts(shoppingCartProducts, clb){
    stockManager.removeReservedProducts(shoppingCartProducts)
        .then(function(){
            clb(null);
        })
        .catch(function(err){
            err._message_ = "Error while removing the reserved products associated with the cart";
            throw err;
        });
}

// endregion

// region Export

module.exports = {
    removeCheckoutProcesses: removeCheckoutProcesses,
    cleanUp: cleanUp
};

// endregion