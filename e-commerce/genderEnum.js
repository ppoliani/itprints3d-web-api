var Enum = require('../utils/enum');

module.exports = new Enum({
    MALE: 0,
    FEMALE: 10,
    RATHER_NOT_SAY: 20
});