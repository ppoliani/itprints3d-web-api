// region Import

var mongoose = require('mongoose');

// endregion

var DeliveryAddressSchema = new mongoose.Schema({
    firstName: {
        type: String
    },

    lastName: {
        type: String
    },

    addressLine1: {
        type: String
    },

    addressLine2: {
        type: String
    },

    houseNumber: {
        type: String
    },

    company: {
        type: String
    },

    city: {
        type: String
    },

    postcode: {
        type: String
    },

    country: {
        type: String
    },

    region: { // i.e. county
        type: String
    },

    phoneNumber: {
        type: String
    }
});

DeliveryAddressSchema.set('toJSON', {
    transform: function(doc, ret){
        ret.id = ret._id;
        delete ret._id;
    }
});

// region Export

module.exports = DeliveryAddressSchema;

// endregion