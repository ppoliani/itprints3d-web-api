// region Import

var mongoose = require('mongoose');

// endregion

var Schema = mongoose.Schema,

    cartProductSchema = new Schema({
        productID: Schema.Types.ObjectId,
        productName: String,
        price: Number,
        costs: {
            postage: Number,
            packing: Number
        },
        category: String,
        quantity: Number
    });

cartProductSchema.set('toJSON', {
    transform: function(doc, ret){
        ret.id = ret._id;
        delete ret._id;
    }
});

// region Export

module.exports = cartProductSchema;

// endregion