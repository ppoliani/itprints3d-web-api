// region Import

var mongoose = require('mongoose'),
    checkoutPhaseEnum = require('../checkoutPhaseEnum');

// endregion

var
    Schema = mongoose.Schema,

    Checkout = new Schema({
        cartID: Schema.Types.ObjectId,

        createdAt: {
            type: Date,
            default: new Date()
        },

        phase: {
            type: Number,
            default: checkoutPhaseEnum.START
        }
    });

// region Export

module.exports = Checkout;

// endregion