// region Import

var mongoose                = require('mongoose'),
    shortid                 = require("shortid"),
    addressSchema           = require('./address'),
    orderHistorySchema      = require('./orderHistory'),
    orderStatusEnum         = require('../orderStatusEnum'),
    paymentEnum             = require('../paymentMethodEnum'),
    shippingCostCalculator  = require('../shippingCostCalculator'),
    cartProductSchema       = require('./cartProduct');

// endregion

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

var Schema = mongoose.Schema,

    OrderSchema = Schema({
        userID: {
            type: String
        },

        customerID: {
            type: String,
            default: shortid.generate()
        },

        orderNumber: {
            type: String,
            default: shortid.generate()
        },

        orderStatus: {
            type: Number,
            default: orderStatusEnum.PROCESSING
        },

        orderPlacedOn: {
            type: Date,
            default: new Date()
        },

        dateModified: {
            type: Date,
            default: new Date()
        },

        userDigitalInfo: {
            ipAddress: {
                type: String
            },

            userAgent: {
                type: String
            },

            acceptLang: {
                type: String
            }
        },

        paymentDetails: {
            title: Number,
            paymentAddress: [addressSchema],
            paymentMethod: {
                type: Number,
                default: paymentEnum.CREDIT_CARD
            },
            paymentFailReason: String
        },

        shippingDetails: {
            title: Number,
            shippingAddress: [addressSchema],
            shippingMethod: {
                type: Number
            },
            shippingDate: Date,
            shippingFailReason: String
        },

        products: [cartProductSchema],

        costs: {
            postage: Number,
            packing: Number
        },

        history: [orderHistorySchema]
    });

OrderSchema.set('toJSON', {
    transform: function(doc, ret){
        ret.id = ret._id;
        ret.paymentDetails.paymentAddress = ret.paymentDetails.paymentAddress[0];
        ret.shippingDetails.shippingAddress = ret.shippingDetails.shippingAddress[0];

        delete ret._id;
    }
});

OrderSchema.methods.getSubtotal = function getSubtotal(){
    return this.products.reduce(function(acc, curr){
        return acc + (curr.price * curr.quantity);
    }, 0);
};

OrderSchema.methods.getShippingCost = function getShippingCost(){
    return shippingCostCalculator.calculateShippingCostForProducts(this.products);
};

OrderSchema.methods.getGrandtotal = function getGrandtotal(){
    return this.getSubtotal() + this.getShippingCost();
};

OrderSchema.methods.toLookupDto = function toLookupDto(){
    return  {
        id: this.id,
        userID: this.userID,
        orderNumber: this.orderNumber,
        orderStatus: this.orderStatus,
        orderPlacedOn: this.orderPlacedOn,
        dateModified: this.dateModified,
        total: this.getGrandtotal()
    };
};

// region Export

module.exports = OrderSchema;

// endregion