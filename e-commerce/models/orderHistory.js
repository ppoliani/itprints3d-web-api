// region Import

var mongoose = require('mongoose');

// endregion

var orderHistorySchema = new mongoose.Schema({
    userID: {
        type: String
    },

    dateAdded: {
        type: Date,
        default: new Date()
    },

    comment: {
        type: String
    },

    orderStatus: {
        type: Number
    },

    customerNotified: {
        type: Boolean
    }
});

// region Export

module.exports = orderHistorySchema;

// endregion