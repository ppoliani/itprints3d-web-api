// region Import

var mongoose                = require('mongoose'),
    cartProductSchema       = require('./cartProduct');

// endregion

var ProductSchema = new mongoose.Schema({
    productName: {
        type: String
    },

    brandName: {
        type: String
    },

    description: {
        type: String
    },

    stockAvailability: {
        type: Number
    },

    price: {
        type: Number
    },

    oldPrice: {
        type: Number
    },

    costs: {
        postage: Number,
        packing: Number
    }
});

// Returns the product object that will be stores in a shopping cart
ProductSchema.methods.getShoppingCartProduct = function getShoppingCartProduct(cartProductInfo){
    return {
        productID: this.id,
        productName: this.productName,
        price: this.price,
        category: 'Printer',
        costs: this.costs,
        quantity: cartProductInfo.quantity
    };
};

// region Export

module.exports = ProductSchema;

// endregion