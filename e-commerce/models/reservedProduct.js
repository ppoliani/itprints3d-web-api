// region Import

var mongoose = require('mongoose');

// endregion

var
    Schema = mongoose.Schema,

    ReservedProduct = new Schema({
        productID: Schema.Types.ObjectId,
        quantity: Number
    });

// region Export

module.exports = ReservedProduct;

// endregion