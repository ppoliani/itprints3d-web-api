// region Import

var mongoose                = require('mongoose'),
    cartProductSchema       = require('./cartProduct');

// endregion

var
    Schema = mongoose.Schema,

    ShoppingCartSchema = new mongoose.Schema({
        userID: String,
        createdAt: { type: Date, default: new Date() },
        cartProducts: [cartProductSchema]
    });

ShoppingCartSchema.set('toJSON', {
    transform: function(doc, ret){
        ret.id = ret._id.toString();
        delete ret._id;

        return ret;
    }
});

// region Export

module.exports = ShoppingCartSchema;

// endregion