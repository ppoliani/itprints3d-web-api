var Enum = require('../utils/enum');

module.exports = new Enum({
    PENDING: 0,
    PROCESSED: 10,
    REFUNDED: 20,
    PROCESSING: 30,
    SHIPPED: 40,
    DELIVERED: 50,
    FAILED: 60
});