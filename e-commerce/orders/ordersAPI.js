// region Import

var async       = require('async'),
    utils       = require('../../api/utils.js'),
    uof         = require('../../data/unitOfWork');

// endregion

// region Inner Fields

var orderRepository = uof.repository('Order');

// endregion

// region Inner Methods

/**
 * GET /orders
 */
function getOrders(req, res){
    orderRepository.query()
        .exec()
        .then(function(orders){
            res.json(utils.createResultObject(orders.length, orders));
        })
        .catch(function(err){
            res.json(utils.createErrorResult('Could not fetch the orders', err.message));
        });
}

/**
 * GET /orders/lookups
 */
function ordersLookups(req, res){
    var orderBy = req.query.$orderby,
        query = orderRepository.query()
            .skip(req.query.$skip)
            .limit(req.query.$top),
        countQuery =  orderRepository.query();

    if(orderBy){
        query = query.sort(orderBy);
    }

    async.parallel({
        orders: function(clb){
            query.exec()
                .then(function(_orders){
                    clb(null, _orders.map(function(order){ return order.toLookupDto(); }));
                })
                .catch(function(err){
                    throw err;
                });
        },

        count: function(clb){
            countQuery.exec()
                .then(function(_orders){
                    clb(null, _orders.length);
                })
                .catch(function(err){
                    throw err;
                });
        }
    }, function(err, results){
        if(err) throw err;

        res.json(utils.createResultObject(results.count, results.orders));
    });
}

/**
 * GET /orders/:id
 */
function getOrderById(req, res){
    orderRepository.findById(req.params.id)
        .then(function(order){
            if(!order){
                res.send(404);
            }
            else {
                res.json(utils.createResultObject(undefined, order));
            }
        })
        .catch(function(err){
            res.json(utils.createErrorResult('An error occured while fetching the order: ', err.message));
        });
}

/**
 * GET /orders/:id
 */
function deleteOrder(req, res){
    throw 'Not Implemented';
}


// endregion

// region Export

module.exports = {
    getOrders: getOrders,
    ordersLookups: ordersLookups,
    getOrderById: getOrderById,
    deleteOrder: deleteOrder
};

// endregion