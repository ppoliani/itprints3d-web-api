var ordersAPI = require('./ordersAPI');

module.exports = {
    'orders': {
        method: 'get',
        fn: ordersAPI.getOrders,
        auth: { scope: '*', claims: { role: ['admin'] } }
    },

    'orders/lookups': {
        method: 'get',
        fn: ordersAPI.ordersLookups,
        auth: { scope: '*', claims: { role: ['admin'] } }
    },

    'orders/:id': {
        method: 'get',
        fn: ordersAPI.getOrderById,
        auth: { scope: '*', claims: { role: ['admin'] } }
    },

    '$orders/:id': {
        method: 'delete',
        fn: ordersAPI.deleteOrder,
        auth: { scope: '*', claims: { role: ['admin'] } }
    }
};