// region Import

var async                   = require('async'),
    assign                  = require('object-assign'),
    logger                  = require('../../logging/logger'),
    utils                   = require('../../api/utils.js'),
    paymentManager          = require('./paymentManager'),
    checkoutProcessManager  = require('../checkoutProcessManager'),
    httpRequestInvestigator = require('../../utils/httpRequestInvestigator');


// endregion

// region Inner Methods


// endregion

// region API Endpoints

/**
 * POST /pay
 */
function pay(req, res){
    var data = assign(req.body, {
            email: req.body.email || req.user.local.email
        }),

        userDigitalInfo = {
            ipAddress: httpRequestInvestigator.ip(req),
            userAgent: httpRequestInvestigator.userAgent(req),
            acceptLang: httpRequestInvestigator.acceptLang(req)
        },

        execSeq = async.compose(
            paymentManager.saveOrderDetails.bind(null, data, userDigitalInfo),
            paymentManager.pay.bind(null, data.paymentDetails),
            paymentManager.changeCheckoutPhase,
            paymentManager.hasPaymentStarted
        );

    execSeq(data.cartID, function(err, order){
        if(err){
            paymentManager.changeCheckoutPhaseAfterError(data.cartID);
            res.json(utils.createErrorResult(err._message_, err.content));
        }
        else {
            res.send(200, order);

            checkoutProcessManager.cleanUp(data.cartID)
                .catch(function(err){
                    logger.error(err);
                });
        }
    });
}

// endregion

// region Export

module.exports = {
    pay: pay
};

// endregion