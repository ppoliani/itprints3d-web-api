// region Import

var async                   = require('async'),
    accounting              = require('accounting'),
    uof                     = require('../../data/unitOfWork'),
    checkoutPhaseEnum       = require('../checkoutPhaseEnum'),
    checkoutProcessManager  = require('../checkoutProcessManager'),
    shoppingCartManager     = require('../shoppingCart/shoppingCartManager'),
    emailManager            = require('../../email/email-manager');

// endregion

// region Inner Fields

var checkoutRepository = uof.repository('CheckoutProcess'),
    orderRepository = uof.repository('Order');

// endregion

// region Inner Methods

/**
 * Checks if payment has already started for the the checkout process
 * associated with the given cartID
 * @param cartID
 */
function hasPaymentStarted(cartID, clb){
    if(!cartID) {
        clb({
            _message_: 'Cannot start payment for an undefined cartID'
        });
    }
    else {
        checkoutRepository.query()
            .where('cartID').equals(cartID)
            .exec()
            .then(function(checkoutProcess){
                if(checkoutProcess[0] && checkoutProcess[0].phase === checkoutPhaseEnum.PAYMENT){
                    clb({
                        _message_: 'Payment is under process for the given cartID: ' + cartID
                    });
                }
                else {
                    clb(null, cartID);
                }
            })
            .catch(function(err){
                err._message_ = 'Error while reading the checkout process for the given id: ' + cartID;
                clb(err);
            });
    }
}

/**
 * Changes the phase of the  checkout process associated with the given cart#
 * @param cartID
 */
function changeCheckoutPhase(cartID, clb){
    checkoutRepository.query()
        .where('cartID').equals(cartID)
        .update({ phase: checkoutPhaseEnum.PAYMENT })
        .exec()
        .then(function(numOfAffectedDocs){
            if(!numOfAffectedDocs){
                clb({
                    _message_: 'No checkout process exist for the given cartID: ' + cartID
                });
            }
            else{
                clb(null);
            }
        })
        .catch(function(err){
            err._message_ = 'Error while changing the checkout process phase';
            clb(err);
        });
}

/**
 * Changes the phase of checkout process after an error has happened
 */
function changeCheckoutPhaseAfterError(cartID){
    return checkoutRepository.query()
        .where('cartID').equals(cartID)
        .update({ phase: checkoutPhaseEnum.FAIL })
        .exec();
}

/**
 * Will contact the payment provider service providing the payment details
 * @param paymentDetails
 * @private
 */
function pay(paymentDetails, clb){
    // ToDo: contact the payment provider service
    clb(null);
}

/**
 * Saves the given save order
 * @param orderDetails
 * @private
 */
function saveOrderDetails(orderDetails, userDigitalInfo, clb){
    var execSeq = async.compose(
        _saveOrder.bind(null, orderDetails, userDigitalInfo),
        _getShoppingCart
    );

    execSeq(orderDetails.cartID, function(err){
        if(err){ clb(err); }
        else { clb(null); }
    });
}

/**
 * Returns the cart with the given id
 * @param cartID
 * @param clb
 * @private
 */
function _getShoppingCart(cartID, clb){
    shoppingCartManager.getShoppingCartById(cartID)
        .then(function(shoppingCart){
            clb(null, shoppingCart);
        })
        .catch(function(err){
            clb(err);
        });
}

/**
 * Save the order details to the db
 * @param orderDetails
 * @param clb
 * @private
 */
function _saveOrder(orderDetails, userDigitalInfo, shoppingCart, clb){
    // ToDo: how to calculate the total postage + packing costs?
    var products = shoppingCart.cartProducts.map(function(product){
            return {
                productID: product.id,
                productName: product.productName,
                price: product.price,
                costs: product.costs,
                quantity: product.quantity
            };
        }),

        order =     {
            userID: shoppingCart.userID,

            userDigitalInfo: userDigitalInfo,

            shippingDetails: {
                title: orderDetails.shippingDetails.title,
                shippingAddress: [orderDetails.shippingDetails.shippingAddress]
            },

            paymentDetails: {
                title: orderDetails.paymentDetails.title,
                paymentAddress: [orderDetails.paymentDetails.paymentAddress]
            },

            products: products
        };


    orderRepository.save(order)
        .then(function(_order_){
            _sendOrderConfirmationEmail(_order_, orderDetails);
            clb(null, _order_);
        })
        .catch(function(err){
            clb(err);
        });
}

/**
 * Sends oreder confirmation email
 * @param order
 * @private
 */
function _sendOrderConfirmationEmail(order, orderDetails){
    emailManager.sendOrderConfirmationEmail({
        email: orderDetails.email,
        customerName: orderDetails.paymentDetails.paymentAddress.firstName,
        orderNumber: order.orderNumber,
        products: order.products.map(function(product){
            // return a new object since product is a schema in which price is Number so we cannot add the currency sign
            return {
                productName: product.productName,
                subtotal: accounting.formatMoney(product.price * product.quantity, '&pound;', 3, ',', '.'),
                price: accounting.formatMoney(product.price, '&pound;', 3, ',', '.'),
                quantity: product.quantity
            };
        }),

        shippingCost: accounting.formatMoney(order.getShippingCost(), '&pound;', 3, ',', '.'),
        subtotal: accounting.formatMoney(order.getSubtotal(), '&pound;', 3, ',', '.'),
        grandtotal: accounting.formatMoney(order.getGrandtotal(), '&pound;', 3, ',', '.')
    });
}

// endregion

// region Export

module.exports = {
    hasPaymentStarted: hasPaymentStarted,
    changeCheckoutPhase: changeCheckoutPhase,
    changeCheckoutPhaseAfterError: changeCheckoutPhaseAfterError,
    pay: pay,
    saveOrderDetails: saveOrderDetails
};

// endregion