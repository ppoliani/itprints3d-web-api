var assign          = require('object-assign'),
    shoppingCartAPI = require('./shoppingCart/shoppingCartAPI'),
    checkoutAPI     = require('./checkout/checkoutAPI'),
    payementAPI     = require('./payment/paymentAPI'),
    orderRoutes     = require('./orders/index').routes;

module.exports = assign(orderRoutes, {
    'shopping-cart': {
        method: 'get',
        fn: shoppingCartAPI.getShoppingCart
    },

    '$shopping-cart': {
        method: 'post',
        fn: shoppingCartAPI.addProduct
    },

    '$$shopping-cart/:id': {
        method: 'delete',
        fn: shoppingCartAPI.deleteCart
    },

    'shopping-cart/:id': {
        method: 'put',
        fn: shoppingCartAPI.updateShoppingCart
    },

    'shopping-cart/:cartID/:productID': {
        method: 'delete',
        fn: shoppingCartAPI.deleteProduct
    },

    'checkout': {
        method: 'post',
        fn: checkoutAPI.checkout
    },

    'pay': {
        method: 'post',
        fn: payementAPI.pay
    }
});