/**
 * Calculates the shipping cost for the given product
 * @param product
 */
function calculateShippingCostForSingleProduct(product){
    // ToDo: request the cost from back end
    return 5;
}

/**
 * Calculates the shipping cost for the given products
 * @param product
 */
function calculateShippingCostForProducts(products){
    return products.reduce(function(acc, curr){
        return acc + calculateShippingCostForSingleProduct(curr);
    }, 0);
}

module.exports = {
    calculateShippingCostForSingleProduct: calculateShippingCostForSingleProduct,
    calculateShippingCostForProducts: calculateShippingCostForProducts
};