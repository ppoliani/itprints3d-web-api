// region Import

var async                   = require('async'),
    logger                  = require('../../logging/logger'),
    uof                     = require('../../data/unitOfWork'),
    utils                   = require('../../api/utils.js'),
    shoppingCartManager     = require('./shoppingCartManager');

// endregion

// region Inner Methods

/**
 * Retrieves the user id from the given request.
 * If user not logged in the IP is used instead
 * @param req
 * @private
 */
function _getUserId(req){
        return req.user
            ? req.user.id
            : req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    }

// endregion

// region API Endpoints

/**
 * GET /shopping-cart
 */
function getShoppingCart(req, res){
    var userId = _getUserId(req);

    shoppingCartManager.getShoppingCart(userId)
        .then(function(cartEntity){
            cartEntity
                ? res.send(200, cartEntity)
                : res.json(200, []);
        })
        .catch(function(err){
            logger.error(err._message_);
            res.send(500, err);
        })
}

/**
 * POST /shopping-cart
 */
function addProduct(req, res){
    var product = req.body.product,
        userId = _getUserId(req);

    product.quantity = Number(product.quantity);

    var execSeq = async.compose(
        shoppingCartManager.isProductInStock.bind(null, product),
        shoppingCartManager.createShoppingCart,
        shoppingCartManager.getShoppingCart
    );


    execSeq(userId, function(err, cart, productInStockResult){
        if(err){
            logger.error(err.message);
            res.send(500, err);
        }
        else {
            if (!productInStockResult.isInStock) {
                res.json(utils.createErrorResult('Not enough products in stock', productInStockResult));
            }

            else {
                shoppingCartManager.updateCart(cart, product)
                    .then(function (cartEntity) {
                        res.send(200, cartEntity);
                    })
                    .catch(function (err) {
                        err._message_ = 'Could not update the shopping cart' + cart.id;
                        logger.error(err._message_);

                        res.json(utils.createErrorResult('Could not update the shopping cart', err));
                    });
            }
        }
    });
}

/**
 * DELETE /shopping-cart/:id
 * @param req
 * @param res
 */
function deleteCart(req, res){
    var exeqSeq = async.compose(
        shoppingCartManager.removeShoppingCart,
        shoppingCartManager.getShoppingCartById
    );

    exeqSeq(req.params.id, function(err){
        if(err){
            res.json(utils.createErrorResult(err._message_));
        }
        else {
            res.send(200);
        }
    });
}

/**
 * DELETE /shopping-cart/:cartID/:productID
 */
function deleteProduct(req, res){
   var productID = req.params.productID,
       cartID = req.params.cartID;

    var exeqSeq = async.compose(
        shoppingCartManager.removeProductFromShoppingCart.bind(null, productID),
        shoppingCartManager.getShoppingCartById
    );

    exeqSeq(cartID, function(err, shoppingCart){
        if(err){
            res.json(utils.createErrorResult(err._message_));
        }
        else {
            uof.save(shoppingCart)
                .then(function(cart){
                    res.send(200, cart);
                })
                .catch(function(err){
                    err._message_ = 'Error while saving the shopping cart: ' + cartID;
                    logger.error(err._message_);

                    res.json(utils.createErrorResult(err._message_));
                });
        }
    });
}

/**
 * UPDATE /shopping-cart:id
 */
function updateShoppingCart(req, res){
    var products = req.body.products,
        stockAvailabilityErrors = [];

    shoppingCartManager.getShoppingCartById(req.params.id)
        .then(function(shoppingCart){
            shoppingCart.cartProducts = []; // remove all products

            var parallel =  shoppingCartManager.getUpdateProductParallelTasks(shoppingCart, products, stockAvailabilityErrors);

            async.parallel(parallel, function(err){
                if(err){
                    throw err;
                }
                else if(stockAvailabilityErrors.length){
                    res.json(utils.createErrorResult('Not enough products in stock', stockAvailabilityErrors));
                }
                else {
                    uof.save(shoppingCart)
                        .then(function(updatedCart){
                            res.send(200, updatedCart);
                        })
                        .catch(function(err){ throw err; });
                }
            });
        })
        .catch(function(err){
            throw err;
        });
}

// endregion

// region Export

module.exports = {
    getShoppingCart: getShoppingCart,
    addProduct: addProduct,
    deleteCart: deleteCart,
    deleteProduct: deleteProduct,
    updateShoppingCart: updateShoppingCart
};

// endregion