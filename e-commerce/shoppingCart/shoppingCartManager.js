// region Import

var Promise                 = require('bluebird'),
    _                       = require('lodash'),
    asyncTask               = require('../../utils/asyncTask'),
    logger                  = require('../../logging/logger'),
    uof                     = require('../../data/unitOfWork'),
    stockManager            = require('../stock/stockManager'),
    checkoutPhaseEnum       = require('../checkoutPhaseEnum'),
    checkoutProcessManager  = require('../checkoutProcessManager');

// endregion

// region Consts

var TTL_EXPIRATION = 86400 * 7; // one week

// endregion

// region Inner Fields

var shoppingCartRepository = uof.repository('ShoppingCart'),
    checkoutRepository = uof.repository('CheckoutProcess');

// endregion

// region Inner Methods

/**
 * Returns the shopping cart with the given id
 * @param cartID
 */
function getShoppingCartById(cartID, clb){
    return asyncTask(function(clb, resolve, reject){
        shoppingCartRepository.findById(cartID)
            .then(function(shoppingCart){
                resolve(shoppingCart);
                clb(null, shoppingCart);
            })
            .catch(function(err){
                reject(err);
                clb(err);
            });
    }, clb);
}

/**
 * returns the shopping cart with the given user id exist
 * @param userId
 * @private
 */
function getShoppingCart(userId, clb){
    return asyncTask(function(clb, resolve, reject){
        shoppingCartRepository.query()
            .where('userID').equals(userId)
            .exec()
            .then(function(cart){
                resolve(cart[0], userId);
                clb(null, cart[0], userId);
            })
            .catch(function(err){
                err._message_ = 'Error while reading an existing shopping cart';

                reject(err);
                clb(err);
            });
    }, clb);
}

/**
 * Creates a new shopping cart if needed
 * @param userId
 * @private
 */
function createShoppingCart(cart, userId, clb){
    if(!cart){ // create a new document
        var shoppingCart = {
            userID: userId,
            cartProducts: []
        };

        shoppingCartRepository.save(shoppingCart)
            .then(function(_shoppingCart_){
                clb(null, _shoppingCart_);
            })
            .catch(function(err){
                err._message_ = 'Error while creating a new shopping cart';
                clb(err);
            });
    }
    else{
        clb(null, cart);
    }
}

/**
 * Checks if product is in stock
 * @param product
 * @param cart
 * @param clb
 * @private
 */
function isProductInStock(product, cart, clb){
    return asyncTask(function(clb, resolve, reject){
        stockManager.checkIfProductInStock(product, _getProductCountInCart(cart, product.productID))
            .then(function(result){
                resolve(result);
                clb(null, cart, result);
            })
            .catch(function(err){
                err._message_ = 'Error while checking the stock availability of the product';

                reject(err);
                clb(err);
            });
    }, clb);
}

/**
 * Returns the number of the given product in the given shopping cart
 * @param cart
 * @param productId
 * @private
 */
function _getProductCountInCart(cart, productId){
    var result = _.find(cart.cartProducts, function(prod){
        return prod.productID.toString() === productId
    });

    return result ? result.quantity : 0;
}

/**
 * Adds the given product to cart or if it already exist then
 * it increments its counter
 * @param cart
 * @param product
 * @private
 */
function updateCart(cart, product){
    return new Promise(function(resolve, reject){
        var productRepository = uof.repository(product.category),

            index = _.findIndex(cart.cartProducts, function(prod){
                return prod.productID.toString() === product.productID;
            });

        if(index !== -1){
            cart.cartProducts[index].quantity += product.quantity;

            uof.save(cart)
                .then(resolve)
                .catch(reject);
        }
        else {
            productRepository.findById(product.productID)
                .then(function(productEntity){
                    cart.cartProducts.push(productEntity.getShoppingCartProduct(product));
                })
                .then(function(){
                    uof.save(cart)
                        .then(resolve)
                        .catch(reject);
                })
                .catch(reject);
        }
    });
}

/**
 * Removes the given cart from the sb
 * @param cart
 */
function removeShoppingCart(cart, clb){
    return asyncTask(function(clb, resolve, reject){
        return uof.remove(cart)
            .then(function(){
                resolve();
                clb();
            })
            .catch(function(err){
                err._message_ = 'Error while removing the shopping cart: ' + cart.id;
                reject(err);
                clb(err);
            });
    }, clb);
}

/**
 * Removes the given product from the specified shopping cart
 * @param productID
 * @param shoppingCart
 * @param clb
 */
function removeProductFromShoppingCart(productID, shoppingCart, clb){
    var index = _.findIndex(shoppingCart.cartProducts, function(prod){
        return prod.productID.toString() === productID;
    });

    if(index !== -1){
        shoppingCart.cartProducts.splice(index, 1);

        uof.save(shoppingCart)
            .then(function(cart){
                clb(null, cart);
            })
            .catch(function(err){
                err._message_ = 'Error while saving the shopping cart: ' + shoppingCart.id;
                clb(err);
            });
    }
    else {
        clb('The product with id: ' + productID + ' was not found in the the shopping cart ' + shoppingCart.id);
    }
}

/**
 * Returns a set of parallel tasks to add a given set of products to the cart
 * @param shoppingCart
 * @param products
 */
function getUpdateProductParallelTasks(shoppingCart, products, stockAvailabilityErrors){
    return products.map(function(product){
        return function(clb){
            var productRepository = uof.repository(product.category);

            isProductInStock(product, shoppingCart)
                .then(function(productInStockResult){
                    if (!productInStockResult.isInStock) {
                        stockAvailabilityErrors.push(productInStockResult);

                        clb();
                    }
                    else {
                        productRepository.findById(product.productID)
                            .then(function(productEntity){
                                shoppingCart.cartProducts.push(productEntity.getShoppingCartProduct(product));
                                clb();
                            })
                            .catch(clb);
                    }
                })
                .catch(clb);
        };
    });
}

// endregion

// region Events

/**
 * Triggered when a shopping cart is removed
 * @param doc
 */
function on_docRemoved(shoppingCart){
    logger.info('Removing expired shopping carts');

    checkoutProcessManager.removeCheckoutProcesses(shoppingCart.id, shoppingCart.cartProducts)
        .catch(function(err){
            logger.error(err);
        });
}

// endregion

// region TTL

/**
 * Checks if the given doc can be removed;
 * It cannot be removed if the checkout process
 * is in the payment phase
 * @param doc
 */
function _canRemove(doc){
    return new Promise(function(resolve, reject){
        checkoutRepository.query()
            .where('cartID').equals(doc.id)
            .exec()
            .then(function(doc){
                resolve(doc.phase !== checkoutPhaseEnum.PAYMENT);
            })
            .catch(function(err){
                reject(err);
            });
    });
}

shoppingCartRepository.setTTL({
    property: 'createdAt',
    expiresAfter: TTL_EXPIRATION,
    onRemove: on_docRemoved,
    canRemove: _canRemove
});

// endregion

// region Export

module.exports = {
    getShoppingCartById: getShoppingCartById,
    getShoppingCart: getShoppingCart,
    createShoppingCart: createShoppingCart,
    isProductInStock: isProductInStock,
    updateCart: updateCart,
    removeProductFromShoppingCart: removeProductFromShoppingCart,
    removeShoppingCart: removeShoppingCart,
    getUpdateProductParallelTasks: getUpdateProductParallelTasks
};

// endregion