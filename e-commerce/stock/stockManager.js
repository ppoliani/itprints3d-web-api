// region Import

var async   = require('async'),
    Promise = require('bluebird'),
    uof     = require('../../data/unitOfWork');

// endregion

// region Inner Fields

var reservedProductsRepository = uof.repository('ReservedProduct');

// endregion

// region Inner Methods

/**
 * Checks if the given products are in stock
 * @param product
 */
function checkIfProductsInStock(products){
    return new Promise(function(resolve, reject){
        var errors = [],
            numOfProducts = products.length;

        products.forEach(function(product){
            checkIfProductInStock(product)
                .then(function(result){
                    numOfProducts -= 1;

                    if(!result.isInStock){
                        errors.push(result);
                    }

                    if(numOfProducts == 0){
                        resolve(errors);
                    }
                })
                .catch(function(err){
                    err._message_ = "Error while checking product availability";
                    reject(err);
                });
        });
    });
}

/**
 * Checks if the given product is in stock
 * @param product
 * @param quantityInCart
 */
function checkIfProductInStock(product, quantityInCart){
    return new Promise(function(resolve, reject){
        var productRepository = uof.repository(product.category);

        productRepository.findById(product.productID)
            .then(function(_product_){
                if(!_product_){
                    reject('There is no product with the given id: ' + product.productID);
                }
                else {
                    _hasEnoughProductsInStock(_product_, quantityInCart, product.quantity)
                        .then(function (hasEnoughProducts) {
                            if (hasEnoughProducts) {
                                resolve({
                                    productID: product.productID,
                                    isInStock: true,
                                    quantityAvailable: _product_.stockAvailability
                                });
                            }
                            else {
                                resolve({
                                    productID: product.productID,
                                    isInStock: false,
                                    quantityAvailable: _product_.stockAvailability
                                });
                            }
                        })
                        .catch(reject);
                }
            })
            .catch(function(err){
                reject(err);
            });
    });
}

/**
 * Adds the given list of products to the reserved collection
 * @param products
 */
function reserveProducts(products){
    return new Promise(function(resolve, reject){
        var parallelTasks =  products.map(function(product){
            return function(clb){
                reservedProductsRepository.findById(product.id)
                    .then(function(reservedProduct){
                        if(reservedProduct){
                            reservedProduct.quantity += 1;

                            uof.save(reservedProduct)
                                .then(function(){ clb(null); })
                                .catch(function(err){  clb(err); });
                        }
                        else {
                            reservedProductsRepository.save({
                                    productID: product.productID,
                                    quantity: product.quantity
                                })
                                .then(function(){ clb(null); })
                                .catch(function(err){ clb(err); });
                        }
                    })
                    .catch(function(err){
                        reject(err);
                    });
            };
        });

        async.parallel(parallelTasks, function(err){
            if(err) reject(err);
            else resolve();
        });
    });
}

/**
 * The opposite functionality of the reserveProducts
 * @param reservedProducts
 */
function removeReservedProducts(reservedProducts){
    return new Promise(function(resolve, reject){
        var $in = reservedProducts.map(function(product){ return product.productID }),
            tasks = [
                __update,
                __remove
            ];

        async.waterfall(tasks, function(err){
            if(err){ reject(err); }
            else{ resolve(); }
        });

        function __update(clb){
            reservedProductsRepository.query()
                .setOptions({ multi: true })
                .where('productID')
                .$in($in)
                .update({ $inc: { quantity: -1 } })
                .exec()
                .then(function(){ clb(null)})
                .catch(function(err){ clb(err); });
        }

        function __remove(clb){
            reservedProductsRepository.query()
                .setOptions({ multi: true })
                .where('quantity').lte(0)
                .remove()
                .exec()
                .then(function(){ clb(null)})
                .catch(function(err){ clb(err); });
        }

    });
}

/**
 * Checks if there are enough items in the stock for this product
 * @param productEntity
 * @param quantityInCart
 * @param quantity
 * @private
 */
function _hasEnoughProductsInStock(productEntity, quantityInCart, quantity) {
    return new Promise(function(resolve, reject){
        quantityInCart = quantityInCart || 0;

        reservedProductsRepository.query()
            .where('productID').equals(productEntity.id)
            .exec()
            .then(function(reservedProduct){
                var reservedProductQuantity = reservedProduct[0] ? reservedProduct[0].quantity : 0;

                resolve(productEntity.stockAvailability - quantityInCart - reservedProductQuantity >= quantity);
            })
            .catch(reject);

    });
}

// endregion

// region Export

module.exports = {
    checkIfProductsInStock: checkIfProductsInStock,
    checkIfProductInStock: checkIfProductInStock,
    reserveProducts: reserveProducts,
    removeReservedProducts: removeReservedProducts
};

// endregion