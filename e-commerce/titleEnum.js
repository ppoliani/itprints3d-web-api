var Enum = require('../utils/enum');

module.exports = new Enum({
    MR: 0,
    MRS: 10,
    MISS: 20,
    MS: 30,
    DR: 40
});