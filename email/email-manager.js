var path           = require('path'),
    templatesDir   = path.resolve(__dirname, './', 'templates'),
    emailTemplates = require('email-templates'),
    nodemailer     = require('nodemailer'),
    logger         = require('../logging/logger');

var FROM = 'It Prints 3D LTD <itprints3d@gmail.com>';

var transport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: 'ppoliani@gmail.com',
        pass: "boliev_20"
    }
});

function _sendEmail(templateName, subject, emailDetails){
    emailTemplates(templatesDir, function(err, template){
        if(err){
            logger.error('Error while loading the email template module: ' + err.message);
        }
        else {
            template(templateName, emailDetails, function(err, html, text){
                if(err){
                    logger.error('Error while loading the email template: ' + err.message);
                }
                else {
                    transport.sendMail({
                        from: FROM,
                        to: emailDetails.email,
                        subject: subject,
                        html: html,
                        text: text
                    }, function(err, responseStatus){
                        if (err) {
                            logger.error('Could not send the email: ' + err.message);
                        }
                    });
                }
            });
        }
    });
}

module.exports = {
    sendWelcomeEmail: _sendEmail.bind(null, 'welcome', 'Welcome to ItPrints3D'),
    sendOrderConfirmationEmail: _sendEmail.bind(null, 'order', 'ItPrints3D Order Confirmation')
};