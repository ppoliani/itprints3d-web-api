// region Import

var mongoose = require('mongoose');

// endregion

var
    Claim = new mongoose.Schema({
        type: String,
        value: String
    }),

    ClaimModel = mongoose.model('Claim', Claim);

// region Export

exports.ClaimSchema = Claim;
exports.ClaimModel = ClaimModel;

// endregion