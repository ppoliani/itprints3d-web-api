
//region Export

module.exports = {
    facebookAuth: {
        clientID: '532347566874164',
        clientSecret: '0448ff645ead826255668fcdb5690340',
        callbackURL: 'http://localhost:9090/auth/facebook/callback'
    },

    twitterAuth : {
        consumerKey 		: 'your-consumer-key-here',
        consumerSecret 	: 'your-client-secret-here',
        callbackURL 		: 'http://localhost:9090/auth/twitter/callback'
    },

    googleAuth : {
        clientID 		: 'your-secret-clientID-here',
        clientSecret 	: 'your-client-secret-here',
        callbackURL 	: 'http://localhost:9090/auth/google/callback'
    }
};

//endregion