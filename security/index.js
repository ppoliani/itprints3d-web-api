
// region Import

var passport         = require('passport'),
    BearerStrategy   = require('passport-http-bearer').Strategy,
    config           = require('../config/index'),
    AccessTokenModel = require('../data').getAuthModel('AccessToken'),
    UserModel        = require('../data').getAuthModel('User');

// endregion

// region Module

/**
 * used to serialize the user for the session
 */
passport.serializeUser(function(user, done){
    done(null, user.id);
});

/**
 * used to deserialize the user
 */
passport.deserializeUser(function(id, done){
    UserModel.findById(id, function(err, user){
        done(err, user);
    });
});

passport.use(new BearerStrategy(
    function(accessToken, done) {
        AccessTokenModel.findOne({ token: accessToken }, function(err, token) {
            if (err) { return done(err); }
            if (!token) { return done(null, false); }

            if(Math.round((Date.now()-token.created)/1000) > config.get('security:tokenLife')) {
                AccessTokenModel.remove({ token: accessToken }, function (err) {
                    if (err) return done(err);
                });

                // ToDo: instead redirect to login page
                return done(null, false, { message: 'Token expired' });
            }

            UserModel.findById(token.userId, function(err, user) {
                if (err) { return done(err); }
                if (!user) { return done(null, false, { message: 'Unknown user' }); }

                // ToDo: scope should be taken from the client
                var info = { scope: '*', claims: user.claims };
                done(null, user, info);
            });
        });
    }
));

// endregion
