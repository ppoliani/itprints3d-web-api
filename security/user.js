
//region Import

var mongoose = require('mongoose'),
    bcrypt   = require('bcrypt-nodejs'),
    ClaimSchema = require('./claim').ClaimSchema;;

//endregion

//region Module

var User = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },

    local: {
        email: {
            type: String,
            unique: true,
            required: true
        },
        hashedPassword: {
            type: String,
            required: true
        },
        salt: {
            type: String,
            required: true
        },
        created: {
            type: Date,
            default: Date.now
        }
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    },

    claims: [ClaimSchema]
});

/**
 * We will be hashing our password within our user model before it saves to the database.
 * This means we don’t have to deal with generating the hash ourselves. It is all handled
 * nicely and neatly inside our user model.
 */
User.methods.encryptPassword = function(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/**
 * Checking if password is valid
 */
User.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.local.password);
};

User.virtual('local.userId')
    .get(function () {
        return this.id;
    });

User.virtual('local.password')
    .set(function(password) {
        this._plainPassword = password;
        this.local.salt = bcrypt.genSaltSync(8);
        //more secure - this.salt = crypto.randomBytes(128).toString('base64');
        this.local.hashedPassword = this.encryptPassword(password);
    })
    .get(function() { return this.local.hashedPassword; });

var UserModel = mongoose.model('User', User);

//endregion

//region Export

module.exports = UserModel;

//endregion