var chai                = require('chai'),
    mongoose            = require('mongoose'),
    utils               = require('./../utils'),
    db                  = require('../../../data/index'),
    uof                 = require('../../../data/unitOfWork'),
    checkoutPhaseEnum   = require('../../../e-commerce/checkoutPhaseEnum');

var expect = chai.expect;

describe('The checkout module: ', function(){
    var
        addToShoppingCartData,
        startCheckoutData = {},
        printerRepository = uof.repository('Printer'),
        cartIDs = [];

    // region Util Methods

    var
        _sendAddToShoppingCartRequest = function _sendAddToShoppingCartRequest(body, statusCode, clb, host){
            host = host || '127.0.0.1';

            utils.request('post', '/api/v1/shopping-cart', body, host)
                .expect(statusCode)
                .end(function(err, response){
                    if(err) {
                        clb(err);
                    }
                    else{
                        cartIDs.push(utils.getResponseContent(response).id);
                        clb(null, response);
                    }
                });
        };

    // endregion

    beforeEach(function(done){
        cartIDs = [];

        // add a printer
        printerRepository.save({
                productName: 'printer',
                model: 'model',
                description: 'product description',
                stockAvailability: 2,
                price: 1.20
            })
            .then(function(prod){
                addToShoppingCartData = {
                    product: {
                        productID: prod.id,
                        category: 'Printer',
                        quantity: 1
                    }
                };

                _sendAddToShoppingCartRequest(addToShoppingCartData, 200, function(err){
                    if(err) {
                        done(err);
                    }
                    else {
                        startCheckoutData.cartID = cartIDs[0];
                        done();
                    }
                });
            })
            .catch(function(err){
                done(err);
            });
    });

    describe('The checkout method', function(){
        it('Given we are starting the checkout process\n' +
            '\t And the checkout has already been started for the same cart\n' +
            '\t When we click checkout\n' +
            '\t Then we should get an http response with the following message:\n' +
            '\t\t "Checkout process has already been started"' +
            '\t\t It should also include the date that it was started', function(done){
                utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err, response){
                    if(err) {
                        done(err);
                    }
                    else {
                        utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err, response){
                            if(err){
                                done(err);
                            }
                            else{
                                expect(utils.getResponseContent(response).message).to.equal('Checkout process has already been started');
                                done();
                            }
                        });
                    }
                });
            });

        it('Given we are starting the checkout process\n' +
            '\t And there is no shopping cart with the given cartID\n' +
            '\t When we click checkout\n' +
            '\t Then we should get an http response with the following message:\n' +
            '\t\t "Error while getting an existing shopping cart"', function(done){
                // Arrange
                startCheckoutData = {
                    cartID: mongoose.Types.ObjectId()
                };

                // Act / Assert
                utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err, response){
                    if(err) {
                        done(err);
                    }
                    else {
                        expect(utils.getResponseContent(response).message).to.equal('Error while getting an existing shopping cart');
                        done();
                    }
                });
        });

        it('Given we are starting the checkout process\n' +
            '\t When we click checkout\n' +
            '\t Then all products from the shopping cart should be\n' +
            '\t\t added to the reserved products collection', function(done){
                utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err){
                    if(err) {
                        done(err);
                    }
                    else {
                        utils.getReservedProducts(function(err, reservedProducts){
                            if(err){
                                done(err); }
                            else{
                                expect(reservedProducts[0].productID.toString()).to.equal(addToShoppingCartData.product.productID);
                                expect(reservedProducts[0].quantity).to.equal(1);
                                done();
                            }
                        });
                    }
                });
        });

        it('Given we are starting the checkout process\n' +
            '\t When we click checkout\n' +
            '\t Then a new checkout process is saved, in the db, for the given cartID;\n' +
            '\t\t and the phase should be set to START', function(done){
                utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err){
                    if(err) {
                        done(err);
                    }
                    else {
                        utils.getCheckoutProcesses(function(err, checkoutProcesses){
                            if(err){
                                done(err); }
                            else{
                                expect(checkoutProcesses[0].cartID.toString()).to.equal(startCheckoutData.cartID);
                                expect(checkoutProcesses[0].phase).to.equal(checkoutPhaseEnum.START );
                                done();
                            }
                        });
                    }
                });
        });

        it('Given we are starting the checkout process\n' +
            '\t And one ore more products in the shopping cart are no more available; probably\n' +
            '\t\t someone else has started checkout before\n' +
            '\t When we click checkout\n' +
            '\t Then we should get an http response with the following message\n:' +
            '\t\t "Stock Availability errors"\n' +
            '\t This will avoid product from going to minus stock availability', function(done){
                // Arrange
                var secondUser = '127.1.1.1';

                addToShoppingCartData.product.quantity = 2;

                // Act / Assert

                // same product added by another user
                _sendAddToShoppingCartRequest(addToShoppingCartData, 200, function(err, response){
                    if(err) {
                        done(err);
                    }
                    else {
                        startCheckoutData.cartID = cartIDs[1];

                        // the second user clicks checkout before the first
                        utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err, response){
                            if(err) {
                                done(err);
                            }
                            else {
                                startCheckoutData.cartID = cartIDs[0];

                                // the first user clicks checkout after than the second; so
                                // he should not ne able to proceed; all stock availability
                                // should be reserved at this point
                                utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err, response){
                                    if(err) {
                                        done(err);
                                    }
                                    else {
                                        expect(utils.getResponseContent(response).message).to.equal('Stock Availability errors');
                                        done();
                                    }
                                });
                            }
                        });
                    }
                }, secondUser);
        });
    });
});