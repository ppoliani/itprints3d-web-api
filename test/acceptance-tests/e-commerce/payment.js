var chai                = require('chai'),
    sinon               = require('sinon'),
    mongoose            = require('mongoose'),
    utils               = require('./../utils'),
    db                  = require('../../../data/index'),
    uof                 = require('../../../data/unitOfWork'),
    checkoutPhaseEnum   = require('../../../e-commerce/checkoutPhaseEnum');


var expect = chai.expect;

describe('The payment module', function(){
    var
        addToShoppingCartData,
        startCheckoutData = {},

        paymentData = {
            email: 'ppoliani@gmail.com',
            shippingDetails: {
                title: 0,
                shippingAddress: {
                    firstName: 'Pavlos',
                    lastName: 'Polianidis',
                    addressLine1: 'St, Lawrence House 29, Abbey square',
                    city: 'Reading',
                    county: 'Berkshire',
                    postcode: 'RG1 3AG',
                    country: 'United Kingdom',
                    phoneNumber: '07702014067'
                }
            },
            paymentDetails: {
                title: 0,
                paymentAddress: {
                    firstName: 'Pavlos',
                    lastName: 'Polianidis',
                    addressLine1: 'St, Lawrence House 29, Abbey square',
                    city: 'Reading',
                    county: 'Berkshire',
                    postcode: 'RG1 3AG',
                    country: 'United Kingdom',
                    phoneNumber: '07702014067'
                }
            }
        },

        printerRepository = uof.repository('Printer'),
        ordersRepository = uof.repository('Order'),
        cartIDs = [];

    // region Util Methods

    var
        _sendAddToShoppingCartRequest = function _sendAddToShoppingCartRequest(body, statusCode, clb, host){
            host = host || '127.0.0.1';

            utils.request('post', '/api/v1/shopping-cart', body, host)
                .expect(statusCode)
                .end(function(err, response){
                    if(err) {
                        clb(err);
                    }
                    else{
                        cartIDs.push(utils.getResponseContent(response).id);
                        clb(null, response);
                    }
                });
        },

        _sendStartPayment = function _sendStartPayment(body, statusCode, clb){
            utils.request('post', '/api/v1/pay', body)
                .expect(statusCode)
                .end(clb);
        },

        _getOrderDetails = function _getOrderDetails(clb){
            ordersRepository.query()
                .where('userID').equals('127.0.0.1')
                .exec()
                .then(function(order){
                    clb(null, order[0]);
                })
                .catch(function(err){
                    clb(err);
                });
        };

    // endregion

    beforeEach(function(done){
        cartIDs = [];

        // add a printer
        printerRepository.save({
                productName: 'printer',
                model: 'model',
                description: 'product description',
                stockAvailability: 2,
                price: 1.20
            })
            .then(function(prod){
                addToShoppingCartData = {
                    product: {
                        productID: prod.id,
                        category: 'Printer',
                        quantity: 1
                    }
                };

                _sendAddToShoppingCartRequest(addToShoppingCartData, 200, function(err){
                    if(err) {
                        done(err);
                    }
                    else {
                        startCheckoutData.cartID = cartIDs[0];
                        paymentData.cartID = cartIDs[0];

                        utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err){
                            if(err) {
                                done(err);
                            }
                            else {
                                done();
                            }
                        });
                    }
                });
            })
            .catch(function(err){
                done(err);
            });
    });

    describe('The pay endpoint', function(){
        it('Given we are starting the payment process\n' +
        '\t And have already clicked pay\n' +
        '\t When we click pay again\n' +
        '\t Then we should get an http response with the following message' +
        '\t\t "Payment is under process for the given cartID"', function(done){
            _sendStartPayment(paymentData, 200, function(err, response){
                if(err){
                    done(err);
                }
                else{
                    _sendStartPayment(paymentData, 200, function(err, response){
                        if(err){
                            done(err);
                        }
                        else {
                            expect(utils.getResponseContent(response).message).to
                                .equal('Payment is under process for the given cartID: ' + paymentData.cartID);
                            done();
                        }
                    });
                }
            });
        });

        it('Given we are starting the payment process\n' +
            '\t And have entered all the payment details\n' +
            '\t When we click pay\n' +
            '\t Then the checkout phase in the db should be PAYMENT', function(done){
                _sendStartPayment(paymentData, 200, function(err, response){
                    if(err){
                        done(err);
                    }
                    else{
                        utils.getCheckoutProcesses(function(err, checkoutProcesses){
                            if(err){
                                done(err);
                            }
                            else {
                                expect(checkoutProcesses[0].phase).to.equal(checkoutPhaseEnum.PAYMENT);
                                done();
                            }
                        });
                    }
                });
            });

        it('Given we are starting the payment process\n' +
            '\t And have entered all the payment details; But for some reason\n' +
            '\t\t there is no checkout process associated with the given cart id\n' +
            '\t When we click pay\n' +
            '\t Then we should get an http response with the following message\n' +
            '\t\t "No checkout process exist for the given cartID"', function(done){
                // Arrange
                paymentData.cartID = mongoose.Types.ObjectId();

                // Act / Assert
                _sendStartPayment(paymentData, 200, function(err, response){
                    if(err){
                        done(err);
                    }
                    else{
                        expect(utils.getResponseContent(response).message).to
                            .equal('No checkout process exist for the given cartID: ' + paymentData.cartID);
                        done();
                    }
                });
            });

        it('Given we are starting the payment process\n' +
            '\t When we click pay\n' +
            '\t Then the order details should be saved to the db', function(done){
                _sendStartPayment(paymentData, 200, function(err, response){
                    if(err){
                        done(err);
                    }
                    else{
                        _getOrderDetails(function(err, order){
                            if(err){
                                done(err);
                            }
                            else {
                                expect(order.userID).to.equal('127.0.0.1');
                                expect(order.shippingDetails.shippingAddress[0].firstName).to.equal('Pavlos');
                                expect(order.shippingDetails.shippingAddress[0].lastName).to.equal('Polianidis');
                                done();
                            }
                        });
                    }
            });
            });

        it('Given we are starting the payment process\n' +
            '\t When we click pay\n' +
            '\t Then checkout process should be removed', function(done){
                _sendStartPayment(paymentData, 200, function(err){
                    if(err){
                        done(err);
                    }
                    else {
                        //add a delay for the cleanUp tasks to be done
                        setTimeout(function(){
                            utils.getCheckoutProcesses(function(err, checkoutProcesses){
                                if(err){
                                    done(err);
                                }
                                else {
                                    expect(checkoutProcesses.length).to.equal(0);
                                    done();
                                }
                            });
                        }, 1000);
                    }
                });
            });

        it('Given we are starting the payment process\n' +
            '\t When we click pay\n' +
            '\t Then shopping cart associated with this transaction should be removed from the db', function(done){
                _sendStartPayment(paymentData, 200, function(err){
                    if(err){
                        done(err);
                    }
                    else {
                        //add a delay for the cleanUp tasks to be done
                        setTimeout(function(){
                            utils.getShoppingCartForCurrentUser(function(err, shoppingCart){
                                if(err){
                                    done(err);
                                }
                                else {
                                    expect(shoppingCart).to.be.undefined;
                                    done();
                                }
                            });
                        }, 1000);
                    }
                });
            });

        it('Given we are starting the payment process\n' +
        '\t When we click pay\n' +
        '\t And there is an error during the payement phase' +
        '\t Then checkout process phase should be changed', function(done){
            paymentData.cartID = undefined;

            _sendStartPayment(paymentData, 200, function(err){
                if(err){
                    done(err);
                }
                else {
                    setTimeout(function(){
                        utils.getCheckoutProcesses(function(err, checkoutProcesses){
                            if(err){
                                done(err);
                            }
                            else {
                                expect(checkoutProcesses[0].phase).to.equal(checkoutPhaseEnum.FAIL);
                                done();
                            }
                        });
                    }, 1000);
                }
            });
        });
    });
});