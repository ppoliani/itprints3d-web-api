var chai    = require('chai'),
    utils   = require('./../utils'),
    db      = require('../../../data/index'),
    uof     = require('../../../data/unitOfWork');

var expect = chai.expect;

describe('The shopping cart module: ', function(){
    var
        data,
        printerRepository = uof.repository('Printer');

    // region Util Methods

    var
        _sendGetShoppingCartRequest = function _sendGetShoppingCartRequest(statusCode, clb){
            utils.request('get', '/api/v1/shopping-cart')
                .expect(statusCode)
                .end(clb);
        },

        _sendAddToShoppingCartRequest = function _sendAddToShoppingCartRequest(body, statusCode, clb){
            utils.request('post', '/api/v1/shopping-cart', body)
                .expect(statusCode)
                .end(clb);
        },

        _getPrinter = function _getPrinter(printerId, clb){
            printerRepository.findById(printerId)
                .then(function(printer){
                    clb(null, printer);
                })
                .catch(function(err){
                    clb(err);
                });
        },

        _sendRemoveFromShoppingCartRequest = function _sendRemoveFromShoppingCartRequest(params, statusCode, clb){
            utils.request('delete', '/api/v1/shopping-cart/' + params.cartID + '/' + params.productID)
                .expect(statusCode)
                .end(clb);
        },

        _sendRemoveShoppingCart = function _sendRemoveShoppingCart(cartID, statusCode, clb){
            utils.request('delete', '/api/v1/shopping-cart/' + cartID)
                .expect(statusCode)
                .end(clb);
        },

        _sendUpdateShoppingCartRequest = function _sendUpdateShoppingCartRequest(body, statusCode, cartID, clb){
            utils.request('put', '/api/v1/shopping-cart/' + cartID, body)
                .expect(statusCode)
                .end(clb);
        };

    //endregion

    beforeEach(function(done){
        // add a printer
        printerRepository.save({
                productName: 'printer',
                model: 'model',
                description: 'product description',
                stockAvailability: 3,
                price: 1.20
            })
            .then(function(prod){
                data = {
                    product: {
                        productID: prod.id,
                        category: 'Printer',
                        quantity: 1
                    }
                };

                done();
            })
            .catch(function(err){
                done(err);
            });
    });

    describe('The get shopping cart method', function(){
        it('Given we have added a product' +
            '\t When we call get shopping cart' +
            '\t Then we should get a shopping cart that includes the previously added product', function(done){
                _sendAddToShoppingCartRequest(data, 200, function(err){
                    if(err){
                        done(err)
                    }
                    else {
                        _sendGetShoppingCartRequest(200, function(err, response){
                            if(err){
                                done(err);
                            }
                            else {
                                expect(utils.getResponseContent(response).cartProducts[0].productID).to.equal(data.product.productID);
                                done();
                            }
                        })
                    }
                });
        });
    });

    describe('The add/update to shopping cart method', function(){
        it('Given we are adding a new product to the shopping cart\n' +
            '\t And there is no cart for the current user in the database\n' +
            '\t When we click add product to shopping cart\n' +
            '\t Then a new shopping cart should be added to the database', function(done){
                _sendAddToShoppingCartRequest(data, 200, function(err){
                    if(err) {
                        done(err);
                    }
                    else {
                        utils.getShoppingCartForCurrentUser(function(err, cart){
                            expect(cart).not.to.be.null;
                            done();
                        });
                    }
                });
        });

        it('Given we are adding a new product to the shopping cart\n' +
            '\t And there is already the same product in the cart\n' +
            '\t When we click add product to shopping cart\n' +
            '\t Then the quantity of the product should be incremented. No new\n' +
            '\t\t product should be saved to the db', function(done){
                _sendAddToShoppingCartRequest(data, 200, function(err){
                    if(err) {
                        done(err);
                    }
                    else {
                        _sendAddToShoppingCartRequest(data, 200, function(err){
                            if(err){
                                done(err)
                            }
                            else {
                                utils.getShoppingCartForCurrentUser(function (err, cart) {
                                    expect(cart.cartProducts.length).to.equal(1);
                                    expect(cart.cartProducts[0].quantity).to.equal(2);
                                    done();
                                })
                            }
                        });
                    }
                });
        });

        it('Given we are adding a new product to the shopping cart\n' +
            '\t And there is already the same product in the cart\n' +
            '\t When we click add product to shopping cart with quantity more than 1 this time\n' +
            '\t Then the quantity of the product should be incremented', function(done){
                _sendAddToShoppingCartRequest(data, 200, function(err){
                    if(err) {
                        done(err);
                    }
                    else {
                        data.product.quantity = 2;

                        _sendAddToShoppingCartRequest(data, 200, function(err){
                            if(err){
                                done(err)
                            }
                            else {
                                utils.getShoppingCartForCurrentUser(function (err, cart) {
                                    expect(cart.cartProducts.length).to.equal(1);
                                    expect(cart.cartProducts[0].quantity).to.equal(3);
                                    done();
                                })
                            }
                        });
                    }
                });
        });

        it('Given we are adding a new product to the shopping cart\n' +
            '\t When we click add product to shopping cart\n' +
            '\t Then the stock availability should not be decremented.\n' +
            '\t\t No products are reserved at this point', function(done){
                _sendAddToShoppingCartRequest(data, 200, function(err, response){
                    if(err) {
                        done(err);
                    }
                    else {
                        _getPrinter(utils.getResponseContent(response).cartProducts[0].productID, function(err, product){
                            expect(product.stockAvailability).to.equal(3);
                            done();
                        });
                    }
                });
            });

        it('Given we are adding a new product to the shopping cart\n' +
            '\t And the requested quantity is more that the stock availability\n' +
            '\t When we click add product to shopping cart\n' +
            '\t Then we should get a response containing the following message:\n' +
            '\t\t "Not enough products in stock"; it should also contain the actual stock availability', function(done){
                // Arrange
                data.product.quantity = 4;

                // Act/Assert
                _sendAddToShoppingCartRequest(data, 200, function(err, response){
                        if(err) {
                            done(err);
                        }
                        else {
                            expect(utils.getResponseContent(response).message).to.equal('Not enough products in stock');
                            expect(utils.getResponseContent(response).error.quantityAvailable).to.equal(3);
                            done()
                        }
                    });
        });

        it('Given we are adding the same product multiple times into the cart (one at the time)\n' +
            '\t And we reach the stock availability\n' +
            '\t When we click add product to shopping cart\n' +
            '\t Then we should get a response containing the following message:\n' +
            '\t\t "Not enough products in stock"; it should also contain the actual stock availability', function(done){
                // Arrange
                data.product.quantity = 2;

                // Act/Assert
                _sendAddToShoppingCartRequest(data, 200, function(err){
                    if(err) {
                        done(err);
                    }
                    else {
                        data.product.quantity = 2;

                        _sendAddToShoppingCartRequest(data, 200, function(err, response){
                            expect(utils.getResponseContent(response).message).to.equal('Not enough products in stock');
                            expect(utils.getResponseContent(response).error.quantityAvailable).to.equal(3);
                            done()
                        });
                    }
                });
        });

        it('Should return 500 if the product is not in the database', function(done){
            // Arrange
            data.product.productID = 'non_existing_id';

            // Act/Assert
            _sendAddToShoppingCartRequest(data, 500, function(err, response){
                if(err) {
                    done(err);
                }
                else {
                    expect(response).not.to.be.null;
                    done();
                }
            });
        });
    });

    describe('The delete product method', function(){
        it('Given we are removing new product to the shopping cart\n' +
        '\t And there are more that one products in the shopping cart already\n' +
        '\t When we click remove product from shopping cart\n' +
        '\t Then the quantity of the given product will be decreased database', function(done){
            _sendAddToShoppingCartRequest(data, 200, function(err, response){
                if(err) {
                    done(err);
                }
                else {
                    var removeProductData = {
                        cartID: utils.getResponseContent(response).id,
                        productID: data.product.productID
                    };

                    _sendRemoveFromShoppingCartRequest(removeProductData, 200, function(err){
                        if(err) {
                            done(err);
                        }
                        else {
                            utils.getShoppingCartForCurrentUser(function(err, cart){
                                if(err) {
                                    done(err);
                                }
                                else {
                                    expect(cart.cartProducts[0]).to.be.undefined;
                                    done();
                                }
                            });
                        }
                    });
                }
            });
        });
    });

    describe('The delete shopping cart method', function(){
        it('Given removing a shopping cart\n' +
        '\t When we click clear shopping cart\n' +
        '\t Then the shopping cart will be removed from the database', function(done){
            _sendAddToShoppingCartRequest(data, 200, function(err, response){
                if(err) {
                    done(err);
                }
                else {
                    _sendRemoveShoppingCart(utils.getResponseContent(response).id, 200, function(err){
                        if(err) {
                            done(err);
                        }
                        else {
                            utils.getShoppingCartForCurrentUser(function(err, cart){
                                if(err) {
                                    done(err);
                                }
                                else {
                                    expect(cart).to.be.undefined;
                                    done();
                                }
                            });
                        }
                    });
                }
            });
        });
    });

    describe('The update products method', function(){
        it('Given we are updating the shopping cart\n' +
            '\t When we click update shopping cart\n' +
            '\t Then shopping cart should contain the updated products', function(done){
                _sendAddToShoppingCartRequest(data, 200, function(err, response){
                    if(err) {
                        done(err);
                    }
                    else {
                        var cartID = utils.getResponseContent(response).id;

                        // update product
                        data.product.quantity = 2;

                        var updateProductsData = {
                            products: [data.product]
                        };

                        _sendUpdateShoppingCartRequest(updateProductsData, 200, cartID, function(err){
                            if(err) {
                                done(err);
                            }
                            else {
                                utils.getShoppingCartForCurrentUser(function(err, cart){
                                    if(err) {
                                        done(err);
                                    }
                                    else {
                                        expect(cart.cartProducts[0].quantity).to.equal(2);
                                        done();
                                    }
                                });
                            }
                        });
                    }
                });
            });

        it('Given we are updating the shopping cart\n' +
            '\t And the new quantity exceeds the stock availability for the given product\n' +
            '\t When we click update shopping cart\n' +
            '\t Then we should get a response containing the following message:\n' +
            '\t\t "Not enough products in stock"; it should also contain the actual stock availability', function(done){
                _sendAddToShoppingCartRequest(data, 200, function(err, response){
                if(err) {
                    done(err);
                }
                else {
                    var cartID = utils.getResponseContent(response).id;

                    // update product
                    data.product.quantity = 4;

                    var updateProductsData = {
                        products: [data.product]
                    };

                    _sendUpdateShoppingCartRequest(updateProductsData, 200, cartID, function(err, responce){
                        if(err) {
                            done(err);
                        }
                        else {
                            expect(utils.getResponseContent(responce).message).to.equal('Not enough products in stock');
                            done();
                        }
                    });
                }
            });
        });

        it('Given we are adding a product to the shopping cart\n' +
        '\t And we have already updated the cart and the the new quantity exceeds the stock availability for the given product\n' +
        '\t When we click update shopping cart\n' +
        '\t Then we should get a response containing the following message:\n' +
        '\t\t "Not enough products in stock"; it should also contain the actual stock availability', function(done){
            _sendAddToShoppingCartRequest(data, 200, function(err, response){
                if(err) {
                    done(err);
                }
                else {
                    var cartID = utils.getResponseContent(response).id;

                    // update product
                    data.product.quantity = 4;

                    var updateProductsData = {
                        products: [data.product]
                    };

                    _sendUpdateShoppingCartRequest(updateProductsData, 200, cartID, function(err, responce){
                        if(err) {
                            done(err);
                        }
                        else {
                            data.product.quantity = 1;
                            _sendAddToShoppingCartRequest(data, 200, function(err, response){
                                if(err) {
                                    done(err);
                                }
                                else {
                                    expect(utils.getResponseContent(responce).message).to.equal('Not enough products in stock');
                                    done();
                                }
                            });
                        }
                    });
                }
            });
        });
    });
});