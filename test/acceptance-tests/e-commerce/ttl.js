var chai                = require('chai'),
    mongoose            = require('mongoose'),
    rewire              = require('rewire'),
    utils               = require('../utils'),
    db                  = require('../../../data/index'),
    uof                 = require('../../../data/unitOfWork'),
    checkoutManager     = rewire('../../../e-commerce/checkout/checkoutManager'),
    shoppingCartManager = rewire('../../../e-commerce/shoppingCart/shoppingCartManager');

var expect  = chai.expect,
    ttlExpiration = 0.5,
    tickInterval = 0.1;

describe('The ttl for e-commerce repositories: \n', function(){
    var printerRepository = uof.repository('Printer'),
        checkoutRepository = uof.repository('CheckoutProcess'),
        shoppingCartRepository = uof.repository('ShoppingCart'),
        addToShoppingCartData,
        startCheckoutData = {},

        paymentData = {
            paymentData: {},

            deliveryAddress: {
                fullName: "Pavlos Polianidis",
                addressLine1: "St, Lawrence House 29, Abbey square",
                city: "Reading",
                county: "Berkshire",
                postcode: "RG1 3AG",
                country: "United Kingdom",
                phoneNumber: "07702014067"
            }
        },

        cartIDs = [];

    // region Utils Methods

    var
        _sendAddToShoppingCartRequest = function _sendAddToShoppingCartRequest(body, statusCode, clb, host){
            host = host || '127.0.0.1';

            utils.request('post', '/api/v1/shopping-cart', body, host)
                .expect(statusCode)
                .end(function(err, response){
                    if(err) {
                        clb(err);
                    }
                    else{
                        cartIDs.push(utils.getResponseContent(response).id);
                        clb(null, response);
                    }
                });
        },

        _sendStartPayment = function _sendStartPayment(body, statusCode, clb){
            utils.request('post', '/api/v1/pay', body)
                .expect(statusCode)
                .end(clb);
        },

        _registerTTL = function registerTTL(){
            checkoutRepository.setTTL({
                property: 'createdAt',
                expiresAfter: ttlExpiration,
                tickInterval: tickInterval,
                onRemove: checkoutManager.__get__('on_docRemoved'),
                canRemove: checkoutManager.__get__('_canRemove')
            });

            shoppingCartRepository.setTTL({
                property: 'createdAt',
                expiresAfter: ttlExpiration,
                tickInterval: tickInterval,
                onRemove: shoppingCartManager.__get__('on_docRemoved'),
                canRemove: shoppingCartManager.__get__('_canRemove')
            });
        };

    //endregion

    beforeEach(function(done){
        cartIDs = [];

        // add a printer
        printerRepository.save({
            productName: 'printer',
            model: 'model',
            description: 'product description',
            stockAvailability: 2,
            price: 1.20
        })
        .then(function(prod){
            addToShoppingCartData = {
                product: {
                    productID: prod.id,
                    category: 'Printer',
                    quantity: 1
                }
            };

            _sendAddToShoppingCartRequest(addToShoppingCartData, 200, function(err){
                if(err) {
                    done(err);
                }
                else {
                    startCheckoutData.cartID = cartIDs[0];
                    paymentData.cartID = cartIDs[0];

                    utils.sendStartCheckoutProcess(startCheckoutData, 200, function(err){
                        if(err) {
                            done(err);
                        }
                        else {
                            done();
                        }
                    });
                }
            });
        })
        .catch(function(err){
            done(err);
        });
    });

    describe('The shopping cart repository ttl', function(){
        it('Given that the ttl registered for the shopping cart repository\n' +
            '\t When the expiration time has passed\n' +
            '\t Then the shopping cart and the corresponding checkout process ' +
            '\t along with reserved data should be removed from the db', function(done){
                // Arrange
                _registerTTL();

                // Act / Assert
                setTimeout(function(){
                    // no reserved products
                    utils.getReservedProducts(function(err, reservedProducts){
                        if(err){
                            done(err);
                        }
                        else {
                            expect(reservedProducts.length).to.equal(0);

                            // no checkout processes
                            utils.getCheckoutProcesses(function(err, checkoutProcesses){
                                if(err){
                                    done(err);
                                }
                                else {
                                    expect(checkoutProcesses.length).to.equal(0);

                                    done();
                                }
                            });
                        }
                    });
                }, 1000);
            });
    });

    describe('The checkout process repository ttl', function(){
        it('Given that the ttl registered for the checkout process repository\n' +
            '\t When the expiration time has passed\n' +
            '\t Then the checkout process along with reserved data should be removed from the db', function(done){
                // Arrange
                _registerTTL();

                // Act / Assrt
                setTimeout(function(){
                    // no reserved products
                    utils.getReservedProducts(function(err, reservedProducts){
                        if(err){
                            done(err);
                        }
                        else {
                            expect(reservedProducts.length).to.equal(0);

                            // no checkout processes
                            utils.getCheckoutProcesses(function(err, checkoutProcesses){
                                if(err){
                                    done(err);
                                }
                                else {
                                    expect(checkoutProcesses.length).to.equal(0);

                                    utils.getShoppingCartForCurrentUser(function(err, shoppingCart){
                                        if(err) {
                                            done(err);
                                        }
                                        else {
                                            expect(shoppingCart).to.be.undefined;
                                            done();
                                        }
                                    });
                                }
                            });
                        }
                    });
                }, 1000);
            });
    })
});