process.env.NODE_ENV = 'test';

var supertest = require('supertest'),
    www       = require('../../bin/www'), // starts the application
    app       = require('../../app'),
    uof       = require('../../data/unitOfWork'),
    db        = require('../../data/index');

beforeEach(function(done){
    function clearDB() {
        db.clearCollections();

        return done();
    }

    clearDB();
});

var shoppingCartRepository = uof.repository('ShoppingCart')
    reservedProductsRepository = uof.repository('ReservedProduct'),
    checkoutRepository = uof.repository('CheckoutProcess');

var
    request = function request(type, url, data, host){
        host = host || '127.0.0.1';

        return supertest(app)[type](url)
            .set('host', host)
            .set('x-forwarded-for', host)
            .send(data)
    },

    getResponseContent = function getResponseContent(response){
        return JSON.parse(response.text);
    },

    getShoppingCartForCurrentUser = function getShoppingCartForCurrentUser(clb){
        shoppingCartRepository.query()
            .where('userID').equals('127.0.0.1')
            .exec()
            .then(function(cart){
                clb(null, cart[0]);
            })
            .catch(function(err){
                clb(err);
            });
    },

    sendStartCheckoutProcess = function sendStartCheckoutProcess(body, statusCode, clb){
        request('post', '/api/v1/checkout', body)
            .expect(statusCode)
            .end(clb);
    },

    getReservedProducts = function getReservedProducts(clb){
        reservedProductsRepository.query()
            .exec()
            .then(function(reservedProducts){
                clb(null, reservedProducts)
            })
            .catch(function(err){
                clb(err)
            });
    },

    getCheckoutProcesses = function getCheckoutProcesses(clb){
        checkoutRepository.query()
            .exec()
            .then(function(docs){
                clb(null, docs);
            })
            .catch(function(err){
                clb(err);
            });
    };

module.exports = {
    request: request,
    getShoppingCartForCurrentUser: getShoppingCartForCurrentUser,
    getResponseContent: getResponseContent,
    sendStartCheckoutProcess: sendStartCheckoutProcess,
    getReservedProducts: getReservedProducts,
    getCheckoutProcesses: getCheckoutProcesses
};