var chai        = require('chai'),
    sinon       = require('sinon'),
    sinonChai   = require('sinon-chai'),
    rewire      = require('rewire'),
    checkoutAPI = rewire('../../../e-commerce/checkout/checkoutAPI'),
    routes      = rewire('../../../e-commerce/routes');

var expect = chai.expect;

chai.use(sinonChai);

describe('The checkout API', function(){
    var _req = {},
        _res = {},
        _utils, _data, _httpResponse, _httpStatusCode,
        _checkoutManager;

    beforeEach(function(){
        _utils = { createErrorResult: sinon.stub() };

        _checkoutManager = {
            checkIfCheckoutExist: sinon.stub(),
            getShoppingCart: sinon.stub(),
            checkProductAvailability: sinon.stub(),
            reserveProducts: sinon.stub(),
            saveCheckoutProcess: sinon.stub()
        };
        checkoutAPI.__set__('utils', _utils);
        checkoutAPI.__set__('checkoutManager', _checkoutManager);

        routes['checkout'].fn = checkoutAPI.checkout;

        _req = {
            user: {
                id: 1234
            },

            body: {
                cartId: 456789
            }
        };

        _res = {
            json: function(data){
                _data = data;
            },
            status: function(statusCode){
              _httpStatusCode = statusCode;

                return this;
            },
            send: function(httpStatusCode){
                _httpResponse = httpStatusCode;
            }
        };
    });

    describe('The checkout endpoint', function(){
        it('Should check if the checkout process already has been started', function(){
            // Act
            routes['checkout'].fn(_req, _res);

            // Assert
            expect(_checkoutManager.checkIfCheckoutExist).to.have.been.called;
        });

        it('Should get the cart contents', function(){
            // Arrange
            _checkoutManager.checkIfCheckoutExist.callsArgWith(1, null, null);

            // Act
            routes['checkout'].fn(_req, _res);

            // Assert
            expect(_checkoutManager.getShoppingCart).to.have.been.called;
        });

        it('Should check the availability of the cart products', function(){
            // Arrange
            _checkoutManager.checkIfCheckoutExist.callsArgWith(1, null, null);

            // Act
            routes['checkout'].fn(_req, _res);

            // Assert
            expect(_checkoutManager.getShoppingCart).to.have.been.called;
        });

        it('Should return an error message to the client, if some products are not in stock anymore', function(){
            // Arrange
            var errorResult = {
                message: 'Not enough products in stock',
                content: [{
                    productId: 56987,
                    quantity: 1
                }]
            };

            _checkoutManager.checkIfCheckoutExist.callsArgWith(1, null, null);
            _checkoutManager.getShoppingCart.callsArgWith(1, null, null);
            _checkoutManager.checkProductAvailability.callsArgWith(1, errorResult);

            _utils.createErrorResult.returns(errorResult);

            // Act
            routes['checkout'].fn(_req, _res);

            // Assert
            expect(_data).to.equal(errorResult);
        });

        it('Should reserve all the products in the cart', function(){
            // Arrange
            _checkoutManager.checkIfCheckoutExist.callsArgWith(1, null, null);
            _checkoutManager.getShoppingCart.callsArgWith(1, null, null);
            _checkoutManager.checkProductAvailability.callsArgWith(1, null, null);

            // Act
            routes['checkout'].fn(_req, _res);

            // Assert
            expect(_checkoutManager.reserveProducts).to.have.been.called;
        });

        it('Should send save the current checkout process to the db', function(){
            // Arrange
            _checkoutManager.checkIfCheckoutExist.callsArgWith(1, null, null);
            _checkoutManager.getShoppingCart.callsArgWith(1, null, null);
            _checkoutManager.checkProductAvailability.callsArgWith(1, null, null);
            _checkoutManager.reserveProducts.callsArgWith(1, null, { id: 1254 });
            _checkoutManager.saveCheckoutProcess.returns({
                then: function(clb){
                    clb();
                    return {
                        catch: function(){}
                    }
                }
            });

            // Act
            routes['checkout'].fn(_req, _res);

            // Assert
            expect(_checkoutManager.saveCheckoutProcess).to.have.been.called;
        });

        it('Should send 200 status code if everything goes right', function(){
            // Arrange
            _checkoutManager.checkIfCheckoutExist.callsArgWith(1, null, null);
            _checkoutManager.getShoppingCart.callsArgWith(1, null, null);
            _checkoutManager.checkProductAvailability.callsArgWith(1, null, null);
            _checkoutManager.reserveProducts.callsArgWith(1, null, { id: 1254 });
            _checkoutManager.saveCheckoutProcess.returns({
                then: function(clb){
                    clb();
                    return {
                        catch: function(){}
                    }
                }
            });

            // Act
            routes['checkout'].fn(_req, _res);

            // Assert
            expect(_httpResponse).to.equal(200);
        });

        it('Should send an error result to the client if checkout process cannot be saved', function(){
            // Arrange
            var errorResult = {
                message: 'Could not save the current checkout process',
                error: ""
            };

            _checkoutManager.checkIfCheckoutExist.callsArgWith(1, null, null);
            _checkoutManager.getShoppingCart.callsArgWith(1, null, null);
            _checkoutManager.checkProductAvailability.callsArgWith(1, null, null);
            _checkoutManager.reserveProducts.callsArgWith(1, null, { id: 1254 });
            _checkoutManager.saveCheckoutProcess.returns({
                then: function(){
                    return {
                        catch: function(clb){ clb(); }
                    }
                }
            });

            _utils.createErrorResult.returns(errorResult);

            // Act
            routes['checkout'].fn(_req, _res);

            // Assert
            expect(_data).to.equal(errorResult);
        });
    });
});