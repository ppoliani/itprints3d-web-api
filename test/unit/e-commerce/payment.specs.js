var chai        = require('chai'),
    sinon       = require('sinon'),
    sinonChai   = require('sinon-chai'),
    rewire      = require('rewire'),
    paymentAPI  = rewire('../../../e-commerce/payment/paymentAPI'),
    routes      = rewire('../../../e-commerce/routes');

var expect = chai.expect;

chai.use(sinonChai);

describe('The Payment API', function(){
    var _req = {},
        _res = {},
        _utils, _checkoutProcessManager,
        _paymentManager, _emailManager,
        _data, _httpResponse, _httpStatusCode;


    beforeEach(function(){
        _utils = { createErrorResult: sinon.stub() };

        _paymentManager = {
            hasPaymentStarted: sinon.stub(),
            changeCheckoutPhase: sinon.stub(),
            pay: sinon.stub(),
            saveOrderDetails: sinon.stub()
        };

        _checkoutProcessManager = {
            cleanUp: sinon.stub(),
            removeCheckoutProcesses: sinon.stub()
        };


        _emailManager = {
            sendWelcomeEmail: sinon.stub(),
            sendOrderConfirmationEmail: sinon.stub()
        };

        paymentAPI.__set__('utils', _utils);
        paymentAPI.__set__('paymentManager', _paymentManager);
        paymentAPI.__set__('checkoutProcessManager', _checkoutProcessManager);


        routes['pay'].fn = paymentAPI.pay;

        _req = {
            user: {
                id: 1234
            },

            body: {
                email: 'example@email.com',
                cartId: 456789
            },

            headers: {
                'x-forwarded-for': 'ip_address',
                'user-agent': '',
                'accept-language': ''
            }
        };

        _res = {
            json: function(data){
                _data = data;
            },
            status: function(statusCode){
                _httpStatusCode = statusCode;

                return this;
            },
            send: function(httpStatusCode){
                _httpResponse = httpStatusCode;
            }
        };
    });

    describe('The pay endpoint', function(){
        it('Should check if payment process has already started for the given shopping cart', function(){
            // Act
            routes['pay'].fn(_req, _res);

            // Assert
            expect(_paymentManager.hasPaymentStarted).to.have.been.called;
        });

        it('Should set the checkout process phase to payment', function(){
            // Arrange
            _paymentManager.hasPaymentStarted.callsArgWith(1, null, null);

            // Act
            routes['pay'].fn(_req, _res);

            // Assert
            expect(_paymentManager.changeCheckoutPhase).to.have.been.called;
        });

        it('Should contact the payment provider', function(){
            // Arrange
            _paymentManager.hasPaymentStarted.callsArgWith(1, null, null);
            _paymentManager.changeCheckoutPhase.callsArgWith(1, null);

            // Act
            routes['pay'].fn(_req, _res);

            // Assert
            expect(_paymentManager.pay).to.have.been.called;
        });

        it('Should save the order details', function(){
            // Arrange
            _paymentManager.hasPaymentStarted.callsArgWith(1, null, null);
            _paymentManager.changeCheckoutPhase.callsArgWith(1, null);
            _paymentManager.pay.callsArgWith(1, null);

            // Act
            routes['pay'].fn(_req, _res);

            // Assert
            expect(_paymentManager.saveOrderDetails).to.have.been.called;
        });

        it('Should send 200 status code if everything goes right', function(){
            // Arrange
            _checkoutProcessManager.cleanUp.returns({
                catch: function(){}
            });

            _paymentManager.hasPaymentStarted.callsArgWith(1, null, null);
            _paymentManager.changeCheckoutPhase.callsArgWith(1, null);
            _paymentManager.pay.callsArgWith(1);
            _paymentManager.saveOrderDetails.callsArgWith(2);

            // Act
            routes['pay'].fn(_req, _res);

            // Assert
            expect(_httpResponse).to.equal(200);
        });

        it('Should remove the checkout process associated with the shopping cart', function(){
            // Arrange
            _checkoutProcessManager.cleanUp.returns({
                catch: function(){}
            });

            _paymentManager.hasPaymentStarted.callsArgWith(1, null, null);
            _paymentManager.changeCheckoutPhase.callsArgWith(1, null);
            _paymentManager.pay.callsArgWith(1, null);
            _paymentManager.saveOrderDetails.callsArgWith(2);

            // Act
            routes['pay'].fn(_req, _res);

            // Assert
            expect(_checkoutProcessManager.cleanUp).to.have.been.called;
        });
    });
});