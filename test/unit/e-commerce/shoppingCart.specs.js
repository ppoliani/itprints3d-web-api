var chai                = require('chai'),
    sinon               = require('sinon'),
    sinonChai           = require('sinon-chai'),
    rewire              = require('rewire'),
    shoppingCartAPI     = rewire('../../../e-commerce/shoppingCart/shoppingCartAPI'),
    routes              = rewire('../../../e-commerce/routes');

var expect = chai.expect;

chai.use(sinonChai);

describe('The shopping cart API', function() {
    var _req = {},
        _res = {},
        _utils, _shoppingCartManager,
        _data, _httpResponse,
        _getUserIdSpy;

    beforeEach(function(){
        _utils = { createErrorResult: sinon.stub() };

        _shoppingCartManager = {
            getShoppingCartById: sinon.stub(),
            getShoppingCart: sinon.stub(),
            createShoppingCart: sinon.stub(),
            isProductInStock: sinon.stub(),
            updateCart: sinon.stub(),
            removeShoppingCart: sinon.stub(),
            removeProductFromShoppingCart: sinon.stub(),
            getUpdateProductParallelTasks: sinon.stub()
        };

        _getUserIdSpy = sinon.spy();

        shoppingCartAPI.__set__('utils', _utils);
        shoppingCartAPI.__set__('shoppingCartManager', _shoppingCartManager);
        shoppingCartAPI.__set__('_getUserId', _getUserIdSpy);

        routes['shopping-cart'].fn = shoppingCartAPI.getShoppingCart;
        routes['$shopping-cart'].fn = shoppingCartAPI.addProduct;
        routes['$$shopping-cart/:id'].fn = shoppingCartAPI.deleteCart;
        routes['shopping-cart/:cartID/:productID'].fn = shoppingCartAPI.deleteProduct;
        routes['shopping-cart/:id'].fn = shoppingCartAPI.updateShoppingCart;

        _req = {
            user: {
                id: 1234
            },

            body: {
                product: {
                    id: 4569,
                    quantity: 2
                }
            },

            params: {}
        };

        _res = {
            json: function(data){
                _data = data;
            },

            send: function(httpStatusCode){
                _httpResponse = httpStatusCode;
            }
        };
    });

    describe('The get shopping cart endpoint', function(){
        it('Should obtain the user id from the incoming request', function(){
            // Arrange
            _shoppingCartManager.getShoppingCart.returns({
                then: function(){
                    return {
                        catch: function(){}
                    }
                }
            });

            // Act
            routes['shopping-cart'].fn(_req, _res);

            // Assert
            expect(_getUserIdSpy).to.have.been.called;
        });

        it('Should return the shopping cart for the current user', function(){
            // Arrange
            _shoppingCartManager.getShoppingCart.returns({
                then: function(){
                    return {
                        catch: function(){}
                    }
                }
            });

            // Act
            routes['shopping-cart'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.getShoppingCart).to.have.been.called;
        });
    });

    describe('The add to cart endpoint', function(){
        it('Should obtain the user id from the incoming request', function(){
            // Act
            routes['$shopping-cart'].fn(_req, _res);

            // Assert
            expect(_getUserIdSpy).to.have.been.called;
        });

        it('Should check if shopping cart exist in the db', function(){
            // Act
            routes['$shopping-cart'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.getShoppingCart).to.have.been.called;
        });

        it('Should create a new shopping cart if one not already exist', function(){
            // Arrange
            _shoppingCartManager.getShoppingCart.callsArgWith(1, null);

            // Act
            routes['$shopping-cart'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.createShoppingCart).to.have.been.called;
        });

        it('Should check if the requested product quantity is in stock', function(){
            // Arrange
            _shoppingCartManager.getShoppingCart.callsArgWith(1, null, null);
            _shoppingCartManager.createShoppingCart.callsArgWith(1, null);

            // Act
            routes['$shopping-cart'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.isProductInStock).to.have.been.called;
        });

        it('Should  respond to the client with an error message if not enough products in stock;' +
            'Additionally the msg should contain the amount of products in stock', function(){
                // Arrange
                var errorResult = {
                    message: 'Not enough products in stock',
                    productInStockResult: 1
                };

                _shoppingCartManager.getShoppingCart.callsArgWith(1, null, null);
                _shoppingCartManager.createShoppingCart.callsArgWith(1, null);
                _shoppingCartManager.isProductInStock.callsArgWith(1, null, null, {
                    isInStock: false,
                    quantityAvailable: 1
                });

                _utils.createErrorResult.returns(errorResult);

                // Act
                routes['$shopping-cart'].fn(_req, _res);

                // Assert
                expect(_data).to.equal(errorResult);
        });

        it('Should update the cart', function(){
            // Arrange
            _shoppingCartManager.getShoppingCart.callsArgWith(1, null, null);
            _shoppingCartManager.createShoppingCart.callsArgWith(1, null);
            _shoppingCartManager.isProductInStock.callsArgWith(1, null, null, {
                isInStock: true,
                quantityAvailable: 10
            });

            _shoppingCartManager.updateCart.returns({
                then: function(){
                    return {
                        catch: function(){}
                    }
                }
            });

            // Act
            routes['$shopping-cart'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.updateCart).to.have.been.called;
        });

        it('Should send a 200 http status code if product is added/update', function(){
            // Arrange
            _shoppingCartManager.getShoppingCart.callsArgWith(1, null, null);
            _shoppingCartManager.createShoppingCart.callsArgWith(1, null);
            _shoppingCartManager.isProductInStock.callsArgWith(1, null, null, {
                isInStock: true,
                quantityAvailable: 10
            });

            _shoppingCartManager.updateCart.returns({
                then: function(clb){
                    clb();
                    return {
                        catch: function(){}
                    }
                }
            });

            // Act
            routes['$shopping-cart'].fn(_req, _res);

            // Assert
            expect(_httpResponse).to.equal(200);
        });

        it('Should send an error result to the client if product is not added/update', function(){
            // Arrange
            var errorResult = {
                message: 'Could not update the shopping cart',
                error: ""
            };

            _shoppingCartManager.getShoppingCart.callsArgWith(1, null, null);
            _shoppingCartManager.createShoppingCart.callsArgWith(1, null);
            _shoppingCartManager.isProductInStock.callsArgWith(1, null, { id: '123' }, {
                isInStock: true,
                quantityAvailable: 10
            });

            _shoppingCartManager.updateCart.returns({
                then: function(){;
                    return {
                        catch: function(clb){ clb({}) }
                    }
                }
            });

            _utils.createErrorResult.returns(errorResult);

            // Act
            routes['$shopping-cart'].fn(_req, _res);

            // Assert
            expect(_data).to.equal(errorResult);
        });

    });

    describe('The delete cart endpoint', function(){
        it('Should find the shopping cart for the requested cart id', function(){
            // Act
            routes['$$shopping-cart/:id'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.getShoppingCartById).to.have.been.called;
        });

        it('Should remove the shopping cart from the db', function(){
            // Arrange
            _shoppingCartManager.getShoppingCartById.callsArgWith(1, null);

            // Act
            routes['$$shopping-cart/:id'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.removeShoppingCart).to.have.been.called;
        });

        it('Should send a 200 http status code if shopping cart is removed', function(){
            // Arrange
            _shoppingCartManager.getShoppingCartById.callsArgWith(1, null);

            // Act
            routes['$$shopping-cart/:id'].fn(_req, _res);

            // Assert
            expect(_httpResponse).to.equal(200);
        });
    });

    describe('The remove from cart endpoint', function(){
        it('Should find the shopping cart for the requested cart id', function(){
            // Act
            routes['shopping-cart/:cartID/:productID'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.getShoppingCartById).to.have.been.called;
        });

        it('Should remove the product id from the shopping cart', function(){
            // Arrange
            _shoppingCartManager.getShoppingCartById.callsArgWith(1, null);

            // Act
            routes['shopping-cart/:cartID/:productID'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.removeProductFromShoppingCart).to.have.been.called;
        });

        it('Should send a 200 http status code if product is removed', function(){
            // Arrange
            _shoppingCartManager.getShoppingCartById.callsArgWith(1, null);

            // Act
            routes['shopping-cart/:cartID/:productID'].fn(_req, _res);

            // Assert
            expect(_httpResponse).to.equal(200);
        });
    });

    describe('The update shopping cart endpoint', function(){
        it('Should get the shopping cart entity from the cartID', function(){
            // Arrange
            _shoppingCartManager.getShoppingCartById.returns({ then: function(){ return this; }, catch: function(){ return this; } });

            // Act
            routes['shopping-cart/:id'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.getShoppingCartById).to.have.been.called;
        });

        it('Should get a set of parallel tasks that will add the products to the cart', function(){
            // Arrange
            _shoppingCartManager.getShoppingCartById.returns({
                then: function(clb){ clb({}); return this; },
                catch: function(){ return this; }
            });

            _shoppingCartManager.getUpdateProductParallelTasks.returns([function(){}]);

            // Act
            routes['shopping-cart/:id'].fn(_req, _res);

            // Assert
            expect(_shoppingCartManager.getUpdateProductParallelTasks).to.have.been.called;
        });
    });
});