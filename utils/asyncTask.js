// region Import

var Promise     = require('bluebird'),
    _           = require('lodash');

// endregion

/**
 * Makes it easy to turn a function to an async; supports both async patterns; promises and callbacks
 * @param task
 * @param clb
 * @returns {Promise}
 */
module.exports = function(task, clb){
    clb = _.isFunction(clb) ? clb : function() {};

    return new Promise(task.bind(null, clb));
};