
// region Inner Methods

/**
 * Converts the given enum value to a string
 */
function toString(val){
    for(var prop in this._enumeration){
        if(this._enumeration[prop] === val){
            return prop;
        }
    }
}

// endregion

// region Ctor

function Enum(enumeration){
    this._enumeration = enumeration;

    for(var prop in enumeration){
        if(enumeration.hasOwnProperty(prop)){
            this[prop] = enumeration[prop];
        }
    }
}

Enum.prototype = {
    constructor: Enum,
    toString: toString
};

// endregion

module.exports = Enum;