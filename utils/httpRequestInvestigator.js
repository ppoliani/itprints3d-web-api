function ip(req){
    return req.headers['x-forwarded-for'] || req.connection.remoteAddress
}

function userAgent(req){
    return req.headers['user-agent'];
}

function acceptLang(req){
    return req.headers['accept-language'];
}

module.exports = {
    ip: ip,
    userAgent: userAgent,
    acceptLang: acceptLang
};